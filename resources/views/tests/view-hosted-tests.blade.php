@extends("layouts.app")
@section("content")
<div class="container">
    <div class="card shadow mb-5 all-tests-card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <span class="">Your Hosted Tests</span>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row" id="show-all-tests">
                @foreach($tests as $test)
                <div class="col-md-6 mt-4">
                    <div class="card">
                            <div class="card-header">
                                <span>{{ $test->name }}</span>
                            </div>
                            <div class="card-body">
                                <p><strong>Total Marks : </strong>{{ $test->total_marks }}</p>
                                <p><strong>Subject : </strong>{{ App\Module::find($test->modules()->first()->id)->subject->name }}</p>
                                <p><strong>Duration : </strong>{{ $test->duration }} Mins</p>
                            </div>
                            <div class="card-footer text-right">
                            <form action="{{ route('tests.hosted') }}" method="POST" class="d-inline-block">
                            @csrf
                                <input type="hidden" name="test_id" value="{{ $test->id }}">
                                <button type="submit" class="btn btn-outline-primary">Show Test Details</button>
                            </form>
                            <form action="{{ route('tests.destroy', $test->id) }}" method="POST" class="d-inline-block ml-2">
                                @csrf
                                @method("DELETE")
                                <button type="submit" class="btn btn-outline-danger">Delete Test</button>
                            </form>
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    if(window.closed){

    }
    $(window).ready(function(){
        if($.cookie("timer") != "NaN"){
            window.history.forward();
            function noBack() {
                window.history.forward();
            }
        }
    });
</script>
@endsection
