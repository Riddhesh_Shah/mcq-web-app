@extends("layouts.app")
@section("page-level-styles")
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
@endsection
@section("content")
<div class="container mb-5">
    <form action="{{route('tests.store')}}" method="POST" id="questions-form">
        @csrf
        <div id="status"></div>
        <div class="card shadow" id="create-test-card">
            <div class="card-header">
                <h5>Create Test</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <div class="form-group">
                            <label for="test-name">Test Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter Test Name" name="name">
                        </div>
                    </div>
                    <div class="col-md-6 mt-4">
                        <div class="form-group">
                            <label for="test-duration">Test Duration (IN MINUTES)</label>
                            <input type="text" class="form-control" id="duration" placeholder="Enter Test Duration In Minutes" name="duration">
                        </div>
                    </div>
                </div>
                <div class = "row">
                    <div class="col-md-12 mt-4">
                        <div class="form-group">
                        <label for="subject_id">Subject</label>
                            <select class="custom-select" id="subject_id" name="subject_id" onChange="onSubjectChange({{$modules}})">
                                <option value="0" selected>Select Subject</option>
                                @foreach($subjects as $subject)
                                <option value="{{ $subject->id }}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div id="modules-container">
                    
                </div>
                
                <!-- <div class="row"> -->
                    <!-- <div class="col-md-12 mt-4">
                        <div class="card">
                            <div class="card-header">
                                <span>Choose option's</span>
                            </div>
                            <div class="card-body">
                                <table id="questions" class="table table-bordered display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Question</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div> -->
                <!-- </div> -->
            </div>
            <div class="card-footer text-right">
                <!-- <div class="d-none" id="questionCollection"></div> -->
                <input type="checkbox" name="wh" id=""><p>Do you want "WH" questions?</p>
                <button class="btn btn-outline-primary" type="submit" id="submit-btn">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
@section("page-level-scripts")
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script>
    var rows_selected = [];
    var checkedIndexes = new Array();
    var oTable;

    $(document).ready(function() {

        oTable = createDataTable(null);

    });

    function onSubjectChange(modules)
    {
        
        modulesContainer = $('#modules-container');
        subjectSelected = $('#subject_id').val();
        var subjectModules = [];
        var count = 0; 
        
        for(i=0;i<modules.length;i++)
        {
            if(modules[i].subject_id == subjectSelected){
                subjectModules[count] = modules[i].name;
                count++;
            }
        }
        var html = "";
        console.log(subjectModules);
        for(i = 0;i<subjectModules.length;i++)
        {
            html += `<div>
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <div class="form-group">
                            <label for="module_id">Module</label>
                            <input type = "text" name="modules[]" class = "form-control" value="${subjectModules[i]}" readOnly></input>
                        </div>
                    </div>
                    <div class="col-md-6 mt-4">
                        <div class="form-group">
                            <label for="marks">Marks</label>
                            <input type = "text" name="marks[]" class = "form-control" value="0"></input>
                        </div>
                    </div>
                </div>
                </div>`;
        }
        $(modulesContainer).html(html);
        
    }

    function createDataTable(result) {
        oTable = $('#questions').DataTable({
            data : result,
            columns: [{
                title: "Question"
            }],
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
            }],
            select: {
                style: 'multi',
            },
            order: [
                [0, 'asc']
            ]
        });

        return oTable;

    }

    function updateDataTable(result){

        oTable.destroy();
        createDataTable(result);

    }

    $(document).delegate(".option", "click", function(e) {
        console.log(this);

        if (!e.target.checked) {

            var checkbox = $(this).find(".checkbox-option");
            console.log(checkbox);
            if (!checkbox.attr('checked') == undefined) {
                console.log('unchecked');
                checkbox.prop('checked', false);
            } else {
                console.log('checked');
                checkbox.prop('checked', true);
            }

        }
    });

    $('#create-test-card').on('change', function(e) {
        if (e.target.id == "module_id") {
            var id = e.target.id;
            var module_id = $('#' + id)[0].value;
            var baseUrl = window.origin;
            var filePath = '/tests/get/question/' + module_id;
            $.ajax({
                url: baseUrl + filePath,
                method: "GET",
                success: function(result) {
                    result = JSON.parse(result).data;
                    updateDataTable(result);
                }
            });
        }
    });

    // $('#submit-btn').on('click', function(e) {

    //     var array = $('.selected');
    //     // var questions = new Array();
    //     for(var i=0;i<array.length;i++){
    //         var text = array[i].innerText;
    //         $('#questionCollection').append(
    //             `<input type='text' name='options[]' class='form-control' value="${text}">`
    //         );
    //     }

    //     var errors = [];
    //     const name = "Name Field is Empty";
    //     const time = "Time Field is Empty";
    //     const subject = "Select a Subject";
    //     const questions = "No questions are selected";
    //     const minimumQuestionCount = 5;
    //     var optionCount = 0;

    //     e.preventDefault();
    //     if ($('#name').val() == "") {
    //         errors.push(name);
    //     }
    //     if ($('#time').val() == "") {
    //         errors.push(time);
    //     }
    //     if (array.length == 0) {
    //         errors.push(questions);
    //     }
    //     if ($('#subject_id').val() == 0) {
    //         errors.push(subject);
    //     }

    //     console.log(errors.length);
    //     if (errors.length == 0) {
    //         $('#questions-form').submit();
    //         $("#status").empty();
    //         document.getElementById('status').innerHTML +=
    //             `<div class="alert alert-success alert-dismissible fade show" role="alert">
    //             Successfully Created A Test
    //             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    //                 <span aria-hidden="true">&times;</span>
    //             </button>
    //         </div>`;
    //     } else {
    //         showErrors(errors);
    //     }
    // });

    function showErrors(errors) {
        $("#status").empty();
        errors.forEach(function(error) {
            document.getElementById('status').innerHTML += `
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                ${error}
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>`
        });
    }
</script>
@endsection
