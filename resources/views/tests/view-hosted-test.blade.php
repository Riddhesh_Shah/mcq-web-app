@extends('layouts.app')
@section("content")
<div class="container">
    <div class="card shadow d-flex justify-content-bewteen">
        <div class="card-header">
            <h4>Title   : <span>{{$test->name}}</span></h4>
            <h4>Total Marks : <span>{{$test->total_marks}}</span></h4>
            <h4>Test Duration : <span>{{$test->duration}} Mins</span></h4>
        </div>
    </div>
    <div class="row mt-5">
        @foreach($test->questions as $question):
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">
                    <span>{{ $question->statement }}</span>
                </div>
                <div class="card-body">
                    @foreach($question->options as $option)
                    <div class="row mb-2">
                        <div class="col-md-12">
                            {{ $option->statement }}
                            @if($option->is_correct == 1)
                                <?php $correctOption = $option->statement?>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="card-footer bg-success">
                    <span class="text-white">Correct Option : {{ $correctOption }}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
