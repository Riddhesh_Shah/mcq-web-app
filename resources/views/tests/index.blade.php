@extends("layouts.app")
@section("content")
<div class="container">
    <div class="card shadow mb-5 all-tests-card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-1 d-flex align-items-center">
                    <span class="">Subjects</span>
                </div>
                <div class="col-md-11 d-flex justify-content-end">
                    <div class="form-group mb-0">
                        <select class="custom-select" id="subject_id" name="subject_id">
                            <option value="0" selected>All</option>
                            @foreach($subjects as $subject)
                            <option value="{{ $subject->id }}">{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row" id="show-all-tests">
                @foreach($tests as $test)
                <?php

                    $test_id = $test->id;
                    $user_id = auth()->user()->id;
                    $test_user_id = Illuminate\Support\Facades\DB::table("test_user")->where("test_id",$test_id)->where("user_id",$user_id)->first();
                ?>
                <div class="col-md-6 mt-4">
                    <div class="card">
                        <form action="{{ route('tests.create.hosted') }}" method="POST">
                        <input type="hidden" name="test_id" value="{{ $test->id }}">
                            @csrf
                            <div class="card-header">
                                <span>{{ $test->name }}</span>
                            </div>
                            <div class="card-body">
                                <p><strong>Total Marks : </strong>{{ $test->total_marks }}</p>
                                <p><strong>Subject : </strong>{{ App\Subject::find($test->modules()->first()->subject_id)->name }}</p>
                                <p><strong>Duration : </strong>{{ $test->duration }} Mins</p>
                            </div>
                            <div class="card-footer text-right">
                                @if($test_user_id == null) 
                                    <button type="submit" class="btn btn-outline-primary">Give Test</button>
                                @else
                                    <span>Test already given</span>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    $(window).ready(function(){
        if($.cookie("timer")){
            $.cookie("timer","",{path: "/", expires : 1/86400});
            window.location.reload(true);
        }
    });
</script>
<script>
    $('.all-tests-card').on('change',function(e){
        if(e.target.id == "subject_id"){
            var id = e.target.id;
            var subject_id = $('#'+id)[0].value;
            var baseUrl = window.origin;
            var filePath = '/tests/get/test/'+subject_id;
            $.ajax({
                url: baseUrl + filePath,
                method: "GET",
                success: function(data){
                    document.getElementById('show-all-tests').innerHTML = "";
                    document.getElementById('show-all-tests').innerHTML = data;
                }
            });
        }
    });
</script>
@endsection