@extends('layouts.app')

@section("content")
<div class="container">
    <div class="card shadow d-flex justify-content-bewteen">
        <div class="card-header">
            <?php
            
            $marksObtained = $test_user->marks_obtained;
            $test = App\Test::find($test_user->test_id);
            $totalMarks = $test->total_marks;
            $title = $test->name;
            $date = date('Y-m-d',strtotime($test_user->created_at));
            $subject = App\Subject::find($test->modules()->first()->subject_id)->name;
            $status = $test_user->status == null ? "Test Abruptly Terminated - Due to Actions of User" : ucfirst($test_user->status);
            $textColor = $test_user == null ? "text-warning" : ($test_user->status == "completed" ? "text-success" : "text-danger");

            ?>
            <h4>Title   : <span>{{$title}}</span></h4>
            <h4>Subject : <span>{{$subject}}</span></h4>
            <h4>Socred  : <span>{{$marksObtained . "/" . $totalMarks}}</span></h4>
            <h4>Date    : <span>{{$date}}</span></h4>
            <h4>Status    : <span class="{{$textColor}}">{{$status}}</span></h4>
        </div>
    </div>
    <div class="row mt-5">
        @foreach($questions_test_user as $question_test_user):
        <?php
        
        $question = App\Question::find($question_test_user->question_id);
        $correctOption = $question->getCorrectOption();
        $selectedOption = App\Option::find($question_test_user->option_id);
        
        ?>
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">
                    <span>{{ $question->statement }}</span>
                </div>
                <div class="card-body">
                    <div class="row p-2">
                        <div class="col-md-2">
                            <span class="">Correct Option</span>
                        </div>
                        <div class="col-md-10">
                            <span>{{ $correctOption->statement }}</span>
                        </div>
                    </div>
                    <div class="row p-2">
                        <div class="col-md-2">
                            <span class="">Selected Option</span>
                        </div>
                        <div class="col-md-10">
                            <span>{{ $selectedOption == null ? "No Option was Selected" : $selectedOption->statement}}</span>
                        </div>
                    </div>
                </div>
                <div class="card-footer {{ $selectedOption == null ? 'bg-danger' : ($correctOption->id == $selectedOption->id ? 'bg-success' : 'bg-danger') }}">
                    <span class="text-white">Your Answer is : {{ $selectedOption == null ? "Wrong" : ($correctOption->id == $selectedOption->id ? "Correct" : "Wrong") }}</span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection