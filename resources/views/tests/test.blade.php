@extends("tests.layouts.app")
@section("Online Test", "page-title")
@section('content')
<div class="container mt-5" id="container">
    <div class="bg-danger p-2 shadow">
        <span class="text-white">Do not refresh the page your test will be terminated</span>
    </div>
    @csrf
    <div class="row height d-flex align-items-center justify-content-center">
        <div class="col-md-4">
            <div>
                <h4 class="total-questions mb-3">Total Questions :
                    <span></span></h4>
                <h4 class="answered-questions mb-3">Answered Questions :
                    <span></span></h4>
                <h4 class="unanswered-questions mb-3">Un-Answered Questions :
                    <span></span></h4>
                <h4 class="unanswered-questions mb-3">Elapsed Time : <span id="remaining-time-in-hrs"></span> : <span id="remaining-time-in-mins"></span> : <span id="remaining-time-in-seconds"></span></h4>
                <div id="time-status">
                </div>
                <div>
                    <form action="{{ route('tests.cancel') }}" method="POST" id="cancel-form">
                        @csrf
                        <button
                            type="button"
                            class="btn btn-danger mb-3"
                            name="cancel_test"
                            id="cancel-test"
                            value="cancel_test">Cancel Your Test</button>
                    </form>
                </div>
                <div>
                    <form action="{{ route('tests.submit') }}" method="POST" id="submit-form">
                            @csrf
                            <button
                                type="button"
                                class="btn btn-success mb-3"
                                name="submit_test"
                                id="submit-test"
                                value="submit_test">Submit Your Test</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8 d-flex align-items-center justify-content-center" id="question-holder">
            
        </div>
        <div class="mt-4">
            <!--This part is optinal and should be removed later, paginate the whole records
            with pagination provided by laravel. JUST FOR REFERENCE-->
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    
                    <li class="page-item disabled">
                        <button
                            class="btn btn-outline-primary"
                            type="button"
                            id="previous"
                            name="previous"
                            value="previous">Previous</button>
                    </li>
                    <li class="page-item ml-3">
                        <button
                            class="btn btn-outline-primary"
                            type="button"
                            id="next"
                            name="next"
                            value="next">Next</button>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
@endsection @section("page-level-scripts")
<script src="{{ asset('js/tests/test.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    $(window).ready(function(){
        if($.cookie("timer") == undefined || $.cookie("timer") == "NaN"){
            var time = {{session()->get("time")}};
            time = parseInt(time);
            $.cookie("timer",time,{path: "/", expires : 10/86400});
            var mins = parseInt((time / 60) % 60);
            var seconds = time % 60;
            if(time < 0){
                time = 0;
            }
            var hrs = parseInt(time / (60 * 60));
            hrs = String(hrs).padStart(2,"0");
            mins = String(mins).padStart(2,"0");
            seconds = String(seconds).padStart(2,"0");
            $('#remaining-time-in-hrs').html(hrs);
            $('#remaining-time-in-mins').html(mins);
            $('#remaining-time-in-seconds').html(seconds);
        }
        setInterval(function(){
            var time = parseInt($.cookie("timer")) - 1;
            $.cookie("timer",time,{path: "/", expires : 10/86400});
            var mins = parseInt((time / 60) % 60);
            var seconds = time % 60;
            if(time < 0){
                time = 0;
            }
            var hrs = parseInt(time / (60 * 60));
            hrs = String(hrs).padStart(2,"0");
            mins = String(mins).padStart(2,"0");
            seconds = String(seconds).padStart(2,"0");
            $('#remaining-time-in-hrs').html(hrs);
            $('#remaining-time-in-mins').html(mins);
            $('#remaining-time-in-seconds').html(seconds);
            if(time == 0){
                $.cookie("timer","NaN",{path: "/"});
                $('#submit-form').submit();
                // console.log(seconds);
                
            }else if(time <= 11){
                // console.log(seconds);
                $('#time-status').html(
                    `<span class='text-danger mb-3'>Test will be Submitted autmatically in : ${seconds}</span>`
                );
            }
        }, 1000);
    });

    $("#submit-test").on("click",function(){
        $.cookie("timer","NaN",{path: "/"});
        $('#submit-form').submit();
    });

    $("#cancel-test").on("click",function(){
        $.cookie("timer","NaN",{path: "/"});
        $('#cancel-form').submit();
    });

    $(document).delegate(".option", "click", function(e){
        $($(this).children()[2]).children()[0].checked = true;
    });

</script>
@endsection