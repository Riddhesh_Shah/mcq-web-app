@extends("layouts.app")
@section("content")
<div class="row mt-5">
        <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Quiz Stats</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Subject</th>
                      <th>Result</th>
                      <th>Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($informations as $information)
                  <?php $test = App\Test::find($information->test_id) ?>
                  <tr>
                      <?php
                        if($information->marks_obtained == null){
                          $marks = App\Test::getMarksObtained($information->id);
                          App\Test::updateMarks($information->id, $marks);
                        }
                      ?>
                      <td>{{ $test->name }}</td>
                      <td>{{ App\Subject::find($test->modules()->first()->subject_id)->name }}</td>
                      <td>{{ $information->marks_obtained == null ? $marks : $information->marks_obtained}}</td>
                      <td>{{ date("Y-m-d",strtotime($information->created_at)) }}</td>
                      <td>
                        <form action="{{ route('tests.view',$information->id) }}" method="POST">
                          @csrf
                          <button class="btn btn-outline-primary" type="submit" value="view">View</button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer d-flex justify-content-center">{{$informations->links()}}</div>
          </div>
        </div>
    </div>
@endsection