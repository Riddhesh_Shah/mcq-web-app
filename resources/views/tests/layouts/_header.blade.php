<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-shadow">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php

use App\Test;
if(session()->has("test_id")){
    $test = Test::find(session()->get("test_id"));
}
            ?>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <h4 class="nav-link">Test Name : {{ $test->name }}</h4>
                </li>
                <li class="nav-item">
                    <h4 class="nav-link">Test time : {{ $test->duration }}min</h4>
                </li>
            </ul>
            <div class="student-name">
                <h4>{{ auth()->user()->name }}</h4>
            </div>
        </div>
</nav>