@extends("layouts.app")
@section("content")
<div class="container">
    <div id="status"></div>
    <div class="card">
        <form action="{{ route('tests.create.random') }}" method="POST" id="random-test-form">
            @csrf
            <input type="hidden" name="test_id" value="">
            <div class="card-header">
                <span>Random Test</span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <select class="custom-select" id="subject_id" name="subject_id">
                            <option value="0" selected>Select Subject</option>
                            @foreach($subjects as $subject)
                                <option value="{{ $subject->id }}">{{$subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <select class="custom-select" id="marks" name="marks">
                            <option value="0" selected>Select Marks</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button class="btn btn-outline-primary" type="button" id="submit-btn">Give Test</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>
    $(window).ready(function(){
        if($.cookie("timer")){
            $.cookie("timer","",{path: "/", expires : 1/86400});
            window.location.reload(true);
        }
    });

    $('#subject_id').on('change',function(e){
        if(this.value == 0){
            $('#marks').html(`
                <option value="0" selected>Select Marks</option>
            `);
        }else{
            var baseUrl = window.origin;
            var filePath = "/tests/get/marks/" + this.value;
            $.ajax({
                url: baseUrl + filePath,
                method: "GET",
                success: function(data){
                    $('#marks').html(data);
                }
            });
        }
    });

    $('#submit-btn').on("click",function(e){
        var errors = [];
        const subject = "Select a Subject";
        const marks = "Select Marks";
        e.preventDefault();
        if($('#subject_id').val() == 0){
            errors.push(subject);
        }
        if($('#marks').val() == 0){
            errors.push(marks);
        }
        if(errors.length == 0){
            console.log('Submitted');
            $('#random-test-form').submit();
        }else{
            console.log('Errors');
            showErrors(errors);
        }
    });

    function showErrors(errors){
        $("#status").empty();
        errors.forEach(function(error){
            document.getElementById('status').innerHTML += `
            <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                ${error}
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'>&times;</span>
                </button>
            </div>`
        });
    }
</script>
@endsection