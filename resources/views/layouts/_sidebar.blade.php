<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion bg-purple" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon">
        <i class="fas fa-bolt"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Quiz Up </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
    Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTest" aria-expanded="true" aria-controls="collapseTest">
        <i class="fas fa-fw fa-pen"></i>
        <span>Test</span>
    </a>
    <div id="collapseTest" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        @can('userOption',App\User::class)
        <a class="collapse-item" href="{{ route('tests.index') }}">Give Hosted Test</a>
        <!-- <a class="collapse-item" href="{{ route('tests.random') }}">Give Random Test</a> -->
        <a class="collapse-item" href="{{ route('tests.previous') }}">Previous Test's</a>
        @endcan
        @canany(['adminOption','hostOption'],App\User::class)
        <a class="collapse-item" href="{{ route('tests.index') }}">Your hosted tests</a>
            <a class="collapse-item" href="{{ route('tests.create') }}">Create Test</a>
        @endcanany
        </div>
    </div>
    </li>

    @can('userOption',App\User::class)
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProfile" aria-expanded="true" aria-controls="collapseProfile">
        <i class="fas fa-fw fa-user"></i>
        <span>Profile</span>
    </a>
    <div id="collapseProfile" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ route('users.edit',auth()->user()->id) }}">Edit Profile</a>
        <a class="collapse-item" href="{{ route('users.show',auth()->user()->id) }}">View Profile</a>
        </div>
    </div>
    </li>
    @endcan

    <!-- @canany(['adminOption','hostOption'],App\User::class)
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseQuestion" aria-expanded="true" aria-controls="collapseQuestion">
        <i class="fas fa-fw fa-user"></i>
        <span>Questions</span>
    </a>
    <div id="collapseQuestion" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ route('questions.create') }}">Add Question</a>
        </div>
    </div>
    </li>
    @endcanany -->

    @canany(['adminOption','hostOption'],App\User::class)
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSubject" aria-expanded="true" aria-controls="collapseSubject">
        <i class="fas fa-fw fa-user"></i>
        <span>Subjects</span>
    </a>
    <div id="collapseSubject" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        @can('adminOption',App\User::class)
        <a class="collapse-item" href="{{ route('subjects.index') }}">All Subjects</a>
        @endcan
        <a class="collapse-item" href="{{ route('subjects.create') }}">Add Subject</a>
        </div>
    </div>
    </li>
    @endcanany

    @canany(['adminOption','hostOption'],App\User::class)
    <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseModule" aria-expanded="true" aria-controls="collapseModule">
        <i class="fas fa-fw fa-user"></i>
        <span>Modules</span>
    </a>
    <div id="collapseModule" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
        @can('adminOption',App\User::class)
        <a class="collapse-item" href="{{ route('modules.index') }}">All Module</a>
        @endcan
        <a class="collapse-item" href="{{ route('modules.create') }}">Add Module</a>
        </div>
    </div>
    </li>
    @endcanany
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>