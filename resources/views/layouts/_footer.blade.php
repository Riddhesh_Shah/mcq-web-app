<footer class="sticky-footer bg-white mt-3">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; <strong>Team Ctrl C + Ctrl V</strong> 2022</span>
        </div>
    </div>
</footer>
