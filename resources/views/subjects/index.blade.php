@extends('layouts.app')

@section('content')

<div class="card shadow">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Subjects To Approve</h6>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead class="">
                <tr>
                    <th scope="col" class="">Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($unapprovedSubjects as $subject)
                <tr>
                    <td>{{$subject->name}}</td>
                    <td>
                        <form action="{{ route('subjects.update',$subject->id) }}" class="d-inline-block mr-3" method="POST">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-outline-success">Approve</button>
                        </form>
                        <form action="{{ route('subjects.destroy',$subject->id) }}" class="d-inline-block" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Unapprove</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="card shadow mt-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Subjects</h6>
    </div>
    <div class="card-body">
        <div class="">
            <table class="table table-bordered">
                <thead class="">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($approvedSubjects as $subject)
                    <tr>
                        <td>{{$subject->name}}</td>
                        <td>
                            <form action="{{ route('subjects.destroy',$subject->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection