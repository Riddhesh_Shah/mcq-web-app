@extends('layouts.app')

@section('page-title')
Create Subject
@endsection

@section('content')

<div class="card shadow">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div class="">
                <p>Subject</p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ route('subjects.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" placeholder="Enter Subject Name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                        @error('name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-md-12 text-center mt-2 d-flex justify-content-end">
                    <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>

            </div>
        </form>
    </div>
</div>

@endsection