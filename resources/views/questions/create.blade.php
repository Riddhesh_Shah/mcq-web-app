@extends('layouts.app')
@section('page-title', "Add Question")
@section('content')
<body class="vh-100">
<div class="loadingWrapper mt-5" top="50%" style="display: none">
        <div class="line">
          <div class="pen">
              <div class="pen_overlay"></div>
              <div class="pen_top"></div>
              <div class="pen_bottom"></div>
          </div>
      </div>
      <span class="loadingSpan">Generating Questions, please wait. Do not reload or close the window</span> 
    </div>
    <div class="container mt-5" id="main_content">
        <input type="hidden" name="" id="module_name" value="Module 0">
        <div class="card">
            <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                <h2>Generate Questions</h2>
            </div>
            <span class="text-danger" id="module_err_msg"></span>
            <div class="card-body pt-2">
                <form action="" enctype="multipart/form-data" method="POST" id="content_form">
                    @csrf
                    <div class="cards">
                        <div class="card mt-3">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="dropdown">
                                        <button class="btn bg-purple text-white dropdown-toggle" type="button"
                                            id="select_content_type" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false">
                                            Select Content Type
                                        </button>
                                        <ul class="dropdown-menu" aria-labelled-by="select_content_type">
                                            <li><a class="dropdown-item" id="display_file_input_field"
                                                    onclick="addFileInput(this)">Video/Audio</a></li>
                                            <li><a class="dropdown-item" id="display_textarea"
                                                    onclick="addTextarea(this)">Text</a></li>
                                            <li><a class="dropdown-item" id="display_link_input_field"
                                                    onclick="addLinkInput(this)">YouTube Link</a></li>
                                        </ul>
                                    </div>
                                    <div class="delete_container">
                                        <a class="delete_button fs-5"  style="font-size:1.3rem;"  onclick="deleteCard(this)"><i
                                                class="fa fa-trash text-danger"></i></a>
                                    </div>
                                </div>
                                <div id="input_type_0">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3 d-flex justify-content-center">
                        <a class="btn bg-purple text-white font-weight-bold fw-bolder fs-3 px-3 py-1 rounded-circle"
                            onclick="addCard()">+</a>
                    </div>

                    <div class="submit_container d-flex justify-content-end">
                        <button type="button" onclick="submitForm(this)" class="btn bg-purple text-white mx-4">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-level-scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{% static 'assets/js/core/popper.min.js' %}"></script>
    <script src = "https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src = "https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
    <script src="{% static 'assets/js/core/bootstrap.min.js' %}"></script>
    <script src="{% static 'assets/js/soft-ui-dashboard.min.js' %}"></script>
    <script src = "{% static 'js/Questions.js' %}"></script>


    <script>


        function showLoadingScreen(){
            console.log(document.getElementsByClassName('loadingWrapper')[0].style.display)
            if( document.getElementsByClassName('loadingWrapper')[0].style.display == 'none' ){
                document.getElementsByClassName('loadingWrapper')[0].style.display = 'block';
                document.getElementById('main_content').style.display = 'none';
            }
            else if( document.getElementsByClassName('loadingWrapper')[0].style.display == 'block'){
                document.getElementsByClassName('loadingWrapper')[0].style.display = 'none';
                document.getElementById('main_content').style.display = 'block';
            }
        }

        function submitForm(){

            // form.preventDefault();
            var form = document.getElementById("content_form");
            
            let module_name = document.getElementById('module_name').value
            module_name = module_name.trim()
            if(module_name == "" || module_name == null){
                document.getElementById('module_name').focus()
                document.getElementById('module_error').innerHTML = 'Please enter a module name'
                return
            }
            console.log("Module:  ", module_name)
            let files = null
            let text_areas = null
            let links = null
            let bigFlag = true
            if(document.getElementsByClassName('files').length == 1){
                files  = document.getElementsByClassName('files')[0].files
                if(files.length == 0){
                    document.getElementById('file_input_error').innerHTML = 'Please select a file'
                    bigFlag=false
                }                
                else{
                    document.getElementById('file_input_error').innerHTML = ''
                }
                let errors = []
                for(let i=0; i<files.length; i++){
                    let file_extension = files[i].name.split('.').pop()
                    console.log(files[i].name,file_extension)
                    let valid_extenions = ['wav','mp4']
                    if(files[i].size/1024/1024 > 1000){
                        errors.push(`File size of file ${files[i].name} exceeds 1000MB`)
                        bigFlag=false
                    }
                    else if(valid_extenions.indexOf(file_extension) == -1){
                        errors.push(`File ${files[i].name} is not a valid file`)
                        bigFlag=false
                    }
                    else{
                        errors.push("")
                    }
                }
                for(let i=0; i<errors.length; i++){
                    if(errors[i] != ""){
                        document.getElementById('file_input_error').innerHTML += errors[i] + '<br>'
                    }
                }
            }else if(document.getElementsByClassName('files').length > 1){
                bigFlag=false
                alert("Please upload all audio/video files in one card only")
            }
            if(document.getElementsByClassName('text_content').length > 0){
                text_areas = document.getElementsByClassName('text_content')
                let flag = true
                for(let i = 0; i < text_areas.length; i++){
                    text_areas[i].value = text_areas[i].value.trim()
                    if(text_areas[i].value == "" || text_areas[i].value == null){
                        let error = text_areas[i].nextElementSibling
                        text_areas[i].focus()
                        error.innerHTML = "Please enter text"
                        flag = false
                    }
                    else if(text_areas[i].value.length < 100){
                        let error = text_areas[i].nextElementSibling
                        text_areas[i].focus()
                        error.innerHTML = "Text should be more than 100 characters"
                        flag = false
                    }
                    else{
                        let error = text_areas[i].nextElementSibling
                        error.innerHTML = ""
                    }
                }
                if(!flag){
                    bigFlag=false
                }
            }
            if(document.getElementsByClassName('link_content').length > 0){
                links = document.getElementsByClassName('link_content')
                let flag = true
                let yt_regex = new RegExp("^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$")
                for(let i = 0; i < links.length; i++){
                    links[i].value = links[i].value.trim()
                    if(links[i].value == "" || links[i].value == null){
                        let error = links[i].nextElementSibling
                        links[i].focus()
                        error.innerHTML = "Please enter a link"
                        flag = false
                    }
                    else if(yt_regex.test(links[i].value) == false){
                        let error = links[i].nextElementSibling
                        links[i].focus()
                        error.innerHTML = "Enter a valid youtube link"
                        flag = false
                    }
                    else{
                        let error = links[i].nextElementSibling
                        error.innerHTML = ""
                    }
                }
                if(!flag){
                    bigFlag=false
                }
            }

            if(!bigFlag){
                return
            }
            if(!files && !text_areas && !links){
                alert("Please add atleast one card")
                return
            }
            formData = new FormData()
            if(files){
                for(let i = 0; i < files.length; i++){
                    formData.append('files', files[i])
                }
            }
            if(text_areas){
                for(let i = 0; i < text_areas.length; i++){
                    formData.append('text[]', text_areas[i].value)
                }
            }
            if(links){
                for(let i = 0; i < links.length; i++){
                    formData.append('links[]', links[i].value)
                }
            }
            formData.append('module_name', module_name)
            
            document.getElementById('module_err_msg').innerHTML = ''


            // if(bigFlag){
            //     form.submit();
            // }
            // showLoadingScreen();
            fetch('http://localhost:4000/api/createQuestions/', {
                method: 'POST',
                headers:{
                    'X-CSRFToken': document.getElementsByName('_token')[0].value
                },
                body: formData
            }).then((response) => {
                console.log('then1')
                if(!response.ok){
                    return response.text().then(text => { throw new Error(text) })
                }
                return response.text()
            }).then((data) => {
                console.log('then2')
                // showLoadingScreen();
                // document.documentElement.innerHTML = ''
                // let dp = new DOMParser()
                // let newHTML = dp.parseFromString(data, 'text/html')
                // document.getElementsByTagName('head')[0].innerHTML = newHTML.getElementsByTagName('head')[0].innerHTML
                // document.getElementsByTagName('body')[0].innerHTML = newHTML.getElementsByTagName('body')[0].innerHTML
                // var table = $('#questions').DataTable();
                // $('#questions_previous').removeClass('page-item');
                // $('#questions_next').removeClass('page-item'); 
                // document.documentElement.innerHTML = data

                localStorage.setItem("data", data);
                window.location.href = window.origin + "/questions/generatedQuestions";

            }).catch((error) =>{
                console.log('catch')
                    document.getElementById('module_err_msg').innerHTML = error
                    // showLoadingScreen()
            })
            
        }



        var count = 0;
        function addFileInput(element) {

            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group mt-2" id="file_input_field">
                                <label>Upload Audio/Video Files <span class="text-danger">(Upload All the files at once)</span></label>
                                <input type="file" class="files form-control" name="files" multiple required>
                                <small class="form-text text-muted">
                                    Files of type mp4, wav are allowed and should be less than 60MB
                                </small><br>
                                <small class="text-danger" id="file_input_error"></small>
                            </div>`);
        }

        function addTextarea(element) {
            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group mt-2">
                                <label>Enter Text</label>
                                <textarea class="form-control text_content" name="text[]" rows="3" required></textarea>
                                <small class="text-danger"></small>
                            </div>`);
        }

        function addLinkInput(element) {
            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group mt-2">
                                <label>YouTube Link</label>
                                <input type="text" class="form-control link_content" name="links[]" placeholder="Enter Link" required>
                                <small class="text-danger"></small>
                            </div>`);
        }


        function addCard() {
            count++;
            $(".cards").append(`<div class="card mt-4">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="dropdown">
                                        <button class="btn bg-purple text-white dropdown-toggle" type="button"
                                            id="select_content_type" aria-haspopup="true" data-toggle="dropdown" aria-expanded="false">
                                            Select Content Type
                                        </button>
                                        <ul class="dropdown-menu" aria-labelled-by="select_content_type">
                                            <li><a class="dropdown-item" id="display_file_input_field"
                                                    onclick="addFileInput(this)">Video/Audio</a></li>
                                            <li><a class="dropdown-item" id="display_textarea"
                                                    onclick="addTextarea(this)">Text</a></li>
                                            <li><a class="dropdown-item" id="display_link_input_field"
                                                    onclick="addLinkInput(this)">YouTube Link</a></li>
                                        </ul>
                                    </div>
                                    <div class="delete_container">
                                        <a class="delete_button fs-5"  style="font-size:1.3rem;"  onclick="deleteCard(this)"><i
                                                class="fa fa-trash text-danger"></i></a>
                                    </div>
                                </div>
                                <div id="input_type_0">
                                </div>
                            </div>
                        </div>`);
        }

        function deleteCard(element) {
            $(element).parent().parent().parent().parent().remove();
        }
    </script>
@endsection


@section('page-level-styles')
<style>
        ::selection {
        background: #515151;
        color: #fff;
        }
        .loadingWrapper{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        margin-top: 10%;
        width: 100%;
        height: 100%;
        background: white;
        font-family: 'Poppins', sans-serif;
        }
        .sign {
        position: absolute;
        bottom: 0;
       right: 128px;
        -webkit-transform: translate(20%, -100%);
        transform: translate(20%, -100%);
        font-size: 2rem;
        color: #fefefe;
        }
        .loadingSpan {
        position: relative;
        top: 50px;
        color: black;
        font-size: 1.2rem;
        font-weight: 700;
        }
        .line {
        position: relative;
        width: 400px;
        height: 6px;
        border-radius: 20px;
        background: #0f0f0f3b;
        }
        .line::before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        width: 0;
        border-radius: 20px;
        height: 100%;
        background: #4458dc;
        -webkit-animation: line_drow 10s ease-out infinite;
        animation: line_drow 10s ease-out infinite;
        -webkit-animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        }
        .line .pen {
        position: absolute;
        top: 0;
        left: 0;
        width: 250px;
        height: 40px;
        line-height: 40px;
        background: rgba(249, 89, 89, 0.07);
        text-align: center;
        -webkit-transform: translate(1%, -365%) rotate(-45deg);
        transform: translate(1%, -365%) rotate(-45deg);
        -webkit-transform-origin: center;
        transform-origin: center;
        -webkit-animation: pen_move 10s ease-out infinite;
        animation: pen_move 10s ease-out infinite;
        -webkit-animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        }
        .line .pen .pen_overlay {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        overflow: hidden;
        }
        .line .pen .pen_overlay::after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: linear-gradient(90deg, #4458dc 0%, #854fee 100%);
        z-index: -1;
        -webkit-animation: color_empty 10s ease-out infinite;
        animation: color_empty 10s ease-out infinite;
        -webkit-animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        }
        .line .pen .pen_overlay::before {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        height: 100%;
        width: 0%;
        background: linear-gradient(90deg, #4458dc 0%, #854fee 100%);
        z-index: -1;
        -webkit-animation: color_full 10s ease-out infinite;
        animation: color_full 10s ease-out infinite;
        -webkit-animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        animation-timing-function: cubic-bezier(0.71, 0.22, 0.29, 0.73);
        }
        .line .pen .pen_top {
        position: absolute;
        top: 0;
        left: 0;
        width: 8%;
        height: 100%;
        -webkit-transform: translateX(-95%);
        transform: translateX(-95%);
        background: #34374c;
        }
        .line .pen .pen_top::before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        height: 8px;
        border-style: solid;
        border-color: transparent #34374c transparent transparent;
        border-width: 16px 35px 16px 0px;
        -webkit-transform: translateX(-101%);
        transform: translateX(-101%);
        }
        .line .pen .pen_top::after {
        content: "";
        position: absolute;
        top: 50%;
        left: -100%;
        width: 10px;
        height: 10px;
        border-radius: 10px 0 0 10px;
        background: #4458dc;
        -webkit-transform: translate(-215%, -50%);
        transform: translate(-215%, -50%);
        }
        .line .pen .pen_bottom {
        position: absolute;
        top: 0;
        right: 0;
        width: 12%;
        height: 102%;
        border-radius: 0 8px 8px 0;
        -webkit-transform: translate(90%, -1%);
        transform: translate(90%, -1%);
        background: #34374c;
        }
        .line .pen p {
        color: #f6f6f6;
        }
        @-webkit-keyframes line_drow {
        0%,
        90%,
        100% {
        width: 0;
        }
        50%,
        60% {
        width: 100%;
        }
        }
        @keyframes line_drow {
        0%,
        90%,
        100% {
        width: 0;
        }
        50%,
        60% {
        width: 100%;
        }
        }
        @-webkit-keyframes color_empty {
        0% {
        left: 0;
        }
        50%,
        60% {
        left: -100%;
        }
        90%,
        100% {
        left: -100%;
        }
        }
        @keyframes color_empty {
        0% {
        left: 0;
        }
        50%,
        60% {
        left: -100%;
        }
        90%,
        100% {
        left: -100%;
        }
        }
        @-webkit-keyframes color_full {
        0% {
        width: 0;
        }
        50%,
        60% {
        width: 0;
        }
        90%,
        100% {
        width: 100%;
        }
        }
        @keyframes color_full {
        0% {
        width: 0;
        }
        50%,
        60% {
        width: 0;
        }
        90%,
        100% {
        width: 100%;
        }
        }
        @-webkit-keyframes pen_move {
        0% {
        left: 0;
        -webkit-transform: translate(1%, -365%) rotate(-45deg);
        transform: translate(1%, -365%) rotate(-45deg);
        }
        50% {
        left: 100%;
        -webkit-transform: translate(1%, -365%) rotate(-45deg);
        transform: translate(1%, -365%) rotate(-45deg);
        }
        55% {
        left: 100%;
        -webkit-transform: translate(1%, -385%) rotate(-45deg);
        transform: translate(1%, -385%) rotate(-45deg);
        }
        60% {
        left: 100%;
        -webkit-transform: translate(-8%, -310%) rotate(140deg);
        transform: translate(-8%, -310%) rotate(140deg);
        }
        90% {
        left: 0%;
        -webkit-transform: translate(-8%, -310%) rotate(140deg);
        transform: translate(-8%, -310%) rotate(140deg);
        }
        95% {
        left: 0%;
        -webkit-transform: translate(-8%, -450%) rotate(140deg);
        transform: translate(-8%, -450%) rotate(140deg);
        }
        100% {
        -webkit-transform: translate(1%, -365%) rotate(315deg);
        transform: translate(1%, -365%) rotate(315deg);
        }
        }
        @keyframes pen_move {
        0% {
        left: 0;
        -webkit-transform: translate(1%, -365%) rotate(-45deg);
        transform: translate(1%, -365%) rotate(-45deg);
        }
        50% {
        left: 100%;
        -webkit-transform: translate(1%, -365%) rotate(-45deg);
        transform: translate(1%, -365%) rotate(-45deg);
        }
        55% {
        left: 100%;
        -webkit-transform: translate(1%, -385%) rotate(-45deg);
        transform: translate(1%, -385%) rotate(-45deg);
        }
        60% {
        left: 100%;
        -webkit-transform: translate(-8%, -310%) rotate(140deg);
        transform: translate(-8%, -310%) rotate(140deg);
        }
        90% {
        left: 0%;
        -webkit-transform: translate(-8%, -310%) rotate(140deg);
        transform: translate(-8%, -310%) rotate(140deg);
        }
        95% {
        left: 0%;
        -webkit-transform: translate(-8%, -450%) rotate(140deg);
        transform: translate(-8%, -450%) rotate(140deg);
        }
        100% {
        -webkit-transform: translate(1%, -365%) rotate(315deg);
        transform: translate(1%, -365%) rotate(315deg);
        }
        }
        
    </style>
@endsection