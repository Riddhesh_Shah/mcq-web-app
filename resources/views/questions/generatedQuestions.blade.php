@extends('layouts.app')
@section('page-title', "Add Question")
@section('content')
<div id="content">
    <form action="{{route('questions.store')}}" method="POST">
        @csrf
        <input type="hidden" name="module_id" id="module_id"></input>
    <div class="question-group">`
    </div>
    <button type = "submit" class = "btn btn-purple text-white">Submit</button>
    </form>
</div>
@endsection
@section('page-level-scripts')
<script>
    window.onload = function(){
        var data = JSON.parse(localStorage.getItem('data')).results;
        console.log(data);
        var html = "";
        for(var i=0;i<data.length;i++){
            // html += data[i].question + " / " + data[i].answer;
            console.log(typeof(data[i].answer));
            var question = data[i].question;
            html += `<div>
    <div class="form-group">
        <div class="d-flex justify-content-between">
            <label for="title">Question</label>
            <div class="d-flex justify-content-between">
                
                <div class="">
                    <button type="button" id="remove-question" class="btn btn-outline-danger ml-3">Remove Question</button>
                </div>
            </div>
        </div>
        <textarea id="question"
        name="question_${i}"
            placeholder="Enter Question"
            class="form-control mt-3" row="3">
            ${question}
            </textarea>
    </div>
    <div class="form-group">
        <div class="container">
        <div class="row" id="question-${i}">
            <input type="hidden" name="" value="1" id="question-${i}-option-count">
            <div class="col-md-6">
                <div class="row" id="question-${i}-option-1">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <input name="q-${i}-a" type="radio" class='correct-option' id="inlineCheckbox1 question-${i}-answer-1" value="1" checked>
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control options" name="q${i}[]" placeholder="Enter Option" value = "${data[i].answer}">
                    </div>
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <i class="fa fa-times cancel-button text-danger" id="option-1"></i>
                    </div>
                </div>
            </div>
            
        </div>
        </div>
    </div>
    <div class="form-group d-flex justify-content-end">
        <button type="button" id="add-option-${i}" class="add-option btn btn-outline-success">Add Options</button>
    </div>
    <hr>
</div>`
        }
        $(".question-group").append(html);
        // $("#content").html(html);
    }
setModuleId();
function setModuleId()
{
    // const collection = document.getElementsByClassName("module_id");
    // console.log(collection.item("0"));
    // collection.forEach(setVals); 
    // console.log() 
    $("#module_id").val(localStorage.getItem("module_id")); 
}


function addSubjects(){
    var options;

    for(var i = 0 ; i < subjects.length ; i++){
        options += `<option value="${subjects[i]["id"]}">${subjects[i]["name"]}</option>`
    }
    return options;
}

$("#add-question").click(function() {
    i++;
    $(".question-group").append(`<div>
    <div class="form-group">
        <div class="d-flex justify-content-between">
            <label for="title">Question</label>
            <div class="d-flex justify-content-between">
                <div class="form-group">
                    <select class="custom-select subject" id="test-subject">
                        <option selected value='select' >Select Subjects</option>
                    `+
                        addSubjects()
                    +`</select>
                </div>
                <div class="">
                    <button type="button" id="remove-question" class="btn btn-outline-danger ml-3">Remove Question</button>
                </div>
            </div>
        </div>
        <input type="text"
            id="question"
            name="question_${i}"
            placeholder="Enter Question"
            value = ""
            class="form-control mt-3">
    </div>
    <div class="form-group">
        <div class="container">
        <div class="row" id="question-${i}">
            <input type="hidden" name="" value="1" id="question-${i}-option-count">
            <div class="col-md-6">
                <div class="row" id="question-${i}-option-1">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <input name="q-${i}-a" type="radio" class='correct-option' id="inlineCheckbox1 question-${i}-answer-1" value="1">
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control options" name="q${i}[]" placeholder="Enter Option" value="${data[i].answer}">
                    </div>
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <i class="fa fa-times cancel-button text-danger" id="option-1"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row" id="question-${i}-option-2">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <input name="q-${i}-a" type="radio" class='correct-option' id="inlineCheckbox1 question-${i}-answer-2" value="2">
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control options" name="q${i}[]" placeholder="Enter Option">
                    </div>
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <i class="fa fa-times cancel-button text-danger" id="option-2"></i>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="form-group d-flex justify-content-end">
        <button type="button" id="add-option-${i}" class="add-option btn btn-outline-success">Add Options</button>
    </div>
    <hr>
</div>`);


});

$(".question-group").click(function(e) {
    if (Object.values(e.target.classList).indexOf("add-option") != -1) {
        $question_id = e.target.id.substr(11);
        $data = $("#question-" + $question_id).children().length-1;
        $optionCount = $data;
        $("#question-" + $question_id).append(`<div class="col-md-6 mt-3">
        <div class="row" id="question-${$question_id}-option-${++$optionCount}">
            <div class="col-md-1 d-flex justify-content-center align-items-center">
                <input name="q-${$question_id}-a" type="radio" class='correct-option' id="inlineCheckbox1" value="${$optionCount}">
            </div>
            <div class="col-md-10">
                <input type="text" class="form-control options" name="q${$question_id}[]" placeholder="Enter Option">
            </div>
            <div class="col-md-1 d-flex justify-content-center align-items-center">
                <i class="fa fa-times cancel-button text-danger" id="option-1"></i>
            </div>
        </div>
    </div>`);
    } else if (
        Object.values(e.target.classList).indexOf("cancel-button") != -1
    ) {
        $(e.target.parentElement.parentElement.parentElement).remove();
    } else if (e.target.id === "remove-question") {
        $(
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement
        ).remove();
    }
});

$("#submit-questions").click(function(e){
    errors = [];
    var questions = new Array;
    var number_of_questions = $(".question-group").children().length;
    var parent = $(".question-group").children();
    for(var i = 0 ; i < number_of_questions ; i++){
        // console.log(getQuestion(parent[i]));
        // console.log(getOptions(parent[i]));
        // console.log(getSubject(parent[i]));
        $question = getQuestion(parent[i]);
        $options = getOptions(parent[i]);
        $module = getModule(parent[i]);
        if(!hasErrors()){
            questions.push(new Array($question, $options, $module));
        }
    }
    if(hasErrors()){
        emptyStatus();
        for(var j = 0 ; j < errors.length ; j++){
             $("#status").append(`
             <div class='col-md-12'>
                 <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                     ${errors[j]}
                     <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                         <span aria-hidden='true'>&times;</span>
                     </button>
                 </div>
             </div>`);
        }
        return false;
     }else{
        alert()
        errors = [];
        emptyStatus();
        console.log(questions);
        $.ajax({
            url: "http://localhost:8000/questions",
            method:"POST",
            data:{
                questions : JSON.stringify(questions),
                _token : $("input[name='_token']").val(),
            },
            success:function(result){
                console.log(result);
                if(result == "success"){
                    window.location.reload(true);
                }
            }
        });
     };
});
</script>
@endsection

