
@extends('layouts.app') @section('content')
<!-- For User -->

@canany(['adminOption','hostOption'], \App\User::class)
<div class="row mt-5">
    <div class="col-md-4 d-flex justify-content-center">
        <div class="card shadow" style="width: 18rem;">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h3 class="card-text">{{$testsCount}}</h3>
                    <i class="fa fa-book fa-3x text-yellow"></i>
                </div>
                <h6 class="card-title mb-1">Created Tests</h6>
            </div>
        </div>
    </div>

    <div class="col-md-4 d-flex justify-content-center">
        <div class="card shadow" style="width: 18rem;">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h3 class="card-text">{{$usersCount}}</h3>
                    <i class="fa fa-user fa-3x text-yellow"></i>
                </div>
                <h6 class="card-title mb-1">Users</h6>
            </div>
        </div>
    </div>

    <div class="col-md-4 d-flex justify-content-center">
        <div class="card shadow" style="width: 18rem;">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <h3 class="card-text">{{$questionsCount}}</h3>
                    <i class="fa fa-pen fa-3x text-yellow"></i>
                </div>
                <h6 class="card-title mb-1">Created Questions</h6>
            </div>
        </div>
    </div>
</div>
@endcanany

<!-- For Admin -->
@can('adminOption', \App\User::class)
<div class="card shadow mt-5">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Users</h6>
    </div>
    <div class="card-body">
        <div class="">
            <table class="table table-bordered">
                <thead class="">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Address</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        <td>
                            <div class="">
                                <a
                                    href="{{ route('users.show',$user->id) }}"
                                    class="btn btn-outline-secondary mr-1">View</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer d-flex justify-content-center">
        {{ $users->links() }}
    </div>
</div>

@endcan @can('userOption', \App\User::class)
<div class="container">
<div class="row mt-5">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Overall Standings</h6>
            </div>
            <div class="card-body">
                <div class="chart-bar">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas
                        id="myBarChart"
                        width="668"
                        height="320"
                        class="chartjs-render-monitor"
                        style="display: block; width: 668px; height: 320px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endcan @endsection @section("page-level-scripts")
@can('userOption', \App\User::class)
<script src="{{ asset('js/vendor/chart.js/Chart.js')}}"></script>
<script>
    var data = {!! json_encode($subjectsWithAverageMarks) !!};
</script>
<script src="{{ asset('js/charts.js')}}"></script>
@endcan
@endsection