@extends('layouts.app')

@section('page-title')
Edit Student
@endsection

@section('content')

<div class="card shadow">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div class="">
                <p>Student</p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{route('users.update', $user->id)}}" method="post">
            @csrf
            @method("PUT")
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" placeholder="Enter Name" value="{{ old('name', $user->name) }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="text" id="email" name="email" placeholder="Enter Email" value="{{ old('email', $user->email) }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
                    @error('email')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="text" id="name" name="phone" placeholder="Enter Name" value="{{ old('phone', $user->phone) }}" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}">
                    @error('phone')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" id="name" name="address" placeholder="Enter Address" value="{{ old('address', $user->address) }}" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}">
                    @error('address')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-12 text-center mt-2 d-flex justify-content-end">
                <button type="submit" class="btn btn-outline-primary">Save</button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection