@extends('layouts.app')

@section('content')

<div class="card shadow">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Modules</h6>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead class="">
                <tr>
                    <th scope="col" class="">Name</th>
                    <th scope="col" class="">Subject</th>
                    <th scope="col" class="">Action</th>

                </tr>
            </thead>
            <tbody>
                @foreach($modules as $module)
                <tr>
                    <td>{{$module->name}}</td>
                    <td>{{$module->subject->name}}</td>
                    <td>
                        <button type="submit" onclick="setModuleId({{$module->id}})" class="btn btn-outline-primary">Generate Questions</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('page-level-scripts')
<script>
    function setModuleId(module)
    {
        localStorage.setItem("module_id",module);
        window.location.href = window.origin + "/questions/create";
    }
</script>
@endsection