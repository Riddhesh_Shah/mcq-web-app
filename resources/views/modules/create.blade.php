@extends('layouts.app')

@section('page-title')
Create Subject
@endsection

@section('content')

<div class="card shadow">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            <div class="">
                <p>Modules</p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ route('modules.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" placeholder="Enter Module Name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                        @error('name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Subject</label>
                        <select class="custom-select subject" id="subject_id" name="subject_id">
                            <option selected value="select">Select Subjects</option>
                            @foreach($subjects as $subject)
                                <option value="{{ $subject->id}}">{{ $subject->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12 text-center mt-2 d-flex justify-content-end">
                    <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>

            </div>
        </form>
    </div>
</div>

@endsection
