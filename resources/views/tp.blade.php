@section('content')
<body class="vh-100">
    <div class="container mt-5" id="main_content">
        <div class="card">
            <div class="card-header p-0 mx-3 mt-3 position-relative z-index-1">
                <h2>Generate Questions</h2>
            </div>
            <span class="text-danger" id="module_err_msg"></span>
            <div class="card-body pt-2">
                <form action="" enctype="multipart/form-data" method="POST" id="content_form">
                    @csrf
                    <div class="cards">
                        <div class="card mt-3">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="dropdown">
                                        <button class="btn bg-gradient-info dropdown-toggle" type="button"
                                            id="select_content_type" data-bs-toggle="dropdown" aria-expanded="false">
                                            Select Content Type
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <li><a class="dropdown-item" id="display_file_input_field"
                                                    onclick="addFileInput(this)">Video/Audio</a></li>
                                            <li><a class="dropdown-item" id="display_textarea"
                                                    onclick="addTextarea(this)">Text</a></li>
                                            <li><a class="dropdown-item" id="display_link_input_field"
                                                    onclick="addLinkInput(this)">YouTube Link</a></li>
                                        </ul>
                                    </div>
                                    <div class="delete_container">
                                        <a class="delete_button fs-5" onclick="deleteCard(this)"><i
                                                class="fa fa-trash text-danger"></i></a>
                                    </div>
                                </div>
                                <div id="input_type_0">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mt-3 d-flex justify-content-center">
                        <a class="btn bg-gradient-secondary fw-bolder fs-3 px-3 py-1 rounded-circle"
                            onclick="addCard()">+</a>
                    </div>

                    <div class="submit_container d-flex justify-content-end">
                        <button type="button" onclick="submitForm()"  class="btn bg-gradient-primary mx-4">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('page-level-styles')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{% static 'assets/js/core/popper.min.js' %}"></script>
    <script src = "https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src = "https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>
    <script src="{% static 'assets/js/core/bootstrap.min.js' %}"></script>
    <script src="{% static 'assets/js/soft-ui-dashboard.min.js' %}"></script>
    <script src = "{% static 'js/Questions.js' %}"></script>


    <script>


        function showLoadingScreen(){
            console.log(document.getElementsByClassName('loadingWrapper')[0].style.display)
            if( document.getElementsByClassName('loadingWrapper')[0].style.display == 'none' ){
                document.getElementsByClassName('loadingWrapper')[0].style.display = 'flex';
                document.getElementById('main_content').style.display = 'none';
                document.getElementById('nav').style.display = 'none';
            }
            else if( document.getElementsByClassName('loadingWrapper')[0].style.display == 'flex'){
                document.getElementsByClassName('loadingWrapper')[0].style.display = 'none';
                document.getElementById('main_content').style.display = 'block';
                document.getElementById('nav').style.display = 'block';
            }
        }

        function submitForm(){
            
            let module_name = document.getElementById('module_name').value
            module_name = module_name.trim()
            if(module_name == "" || module_name == null){
                document.getElementById('module_name').focus()
                document.getElementById('module_error').innerHTML = 'Please enter a module name'
                return
            }
            console.log("Module:  ", module_name)
            let files = null
            let text_areas = null
            let links = null
            let bigFlag = true
            if(document.getElementsByClassName('files').length == 1){
                files  = document.getElementsByClassName('files')[0].files
                if(files.length == 0){
                    document.getElementById('file_input_error').innerHTML = 'Please select a file'
                    bigFlag=false
                }                
                else{
                    document.getElementById('file_input_error').innerHTML = ''
                }
                let errors = []
                for(let i=0; i<files.length; i++){
                    let file_extension = files[i].name.split('.').pop()
                    console.log(files[i].name,file_extension)
                    let valid_extenions = ['wav','mp4']
                    if(files[i].size/1024/1024 > 60){
                        errors.push(`File size of file ${files[i].name} exceeds 60MB`)
                        bigFlag=false
                    }
                    else if(valid_extenions.indexOf(file_extension) == -1){
                        errors.push(`File ${files[i].name} is not a valid file`)
                        bigFlag=false
                    }
                    else{
                        errors.push("")
                    }
                }
                for(let i=0; i<errors.length; i++){
                    if(errors[i] != ""){
                        document.getElementById('file_input_error').innerHTML += errors[i] + '<br>'
                    }
                }
            }else if(document.getElementsByClassName('files').length > 1){
                bigFlag=false
                alert("Please upload all audio/video files in one card only")
            }
            if(document.getElementsByClassName('text_content').length > 0){
                text_areas = document.getElementsByClassName('text_content')
                let flag = true
                for(let i = 0; i < text_areas.length; i++){
                    text_areas[i].value = text_areas[i].value.trim()
                    if(text_areas[i].value == "" || text_areas[i].value == null){
                        let error = text_areas[i].nextElementSibling
                        text_areas[i].focus()
                        error.innerHTML = "Please enter text"
                        flag = false
                    }
                    else if(text_areas[i].value.length < 100){
                        let error = text_areas[i].nextElementSibling
                        text_areas[i].focus()
                        error.innerHTML = "Text should be more than 100 characters"
                        flag = false
                    }
                    else{
                        let error = text_areas[i].nextElementSibling
                        error.innerHTML = ""
                    }
                }
                if(!flag){
                    bigFlag=false
                }
            }
            if(document.getElementsByClassName('link_content').length > 0){
                links = document.getElementsByClassName('link_content')
                let flag = true
                let yt_regex = new RegExp("^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$")
                for(let i = 0; i < links.length; i++){
                    links[i].value = links[i].value.trim()
                    if(links[i].value == "" || links[i].value == null){
                        let error = links[i].nextElementSibling
                        links[i].focus()
                        error.innerHTML = "Please enter a link"
                        flag = false
                    }
                    else if(yt_regex.test(links[i].value) == false){
                        let error = links[i].nextElementSibling
                        links[i].focus()
                        error.innerHTML = "Enter a valid youtube link"
                        flag = false
                    }
                    else{
                        let error = links[i].nextElementSibling
                        error.innerHTML = ""
                    }
                }
                if(!flag){
                    bigFlag=false
                }
            }

            if(!bigFlag){
                return
            }
            if(!files && !text_areas && !links){
                alert("Please add atleast one card")
                return
            }
            formData = new FormData()
            if(files){
                for(let i = 0; i < files.length; i++){
                    formData.append('files', files[i])
                }
            }
            if(text_areas){
                for(let i = 0; i < text_areas.length; i++){
                    formData.append('text[]', text_areas[i].value)
                }
            }
            if(links){
                for(let i = 0; i < links.length; i++){
                    formData.append('links[]', links[i].value)
                }
            }
            formData.append('module_name', module_name)
            
            document.getElementById('module_err_msg').innerHTML = ''
            showLoadingScreen();
            
            fetch('', {
                method: 'POST',
                headers:{
                    'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
                },
                body: formData
            }).then((response) => {
                console.log('then1')
                if(!response.ok){
                    return response.text().then(text => { throw new Error(text) })
                }
                return response.text()
            }).then((data) => {
                console.log('then2')
                showLoadingScreen();
                document.documentElement.innerHTML = ''
                let dp = new DOMParser()
                let newHTML = dp.parseFromString(data, 'text/html')
                document.getElementsByTagName('head')[0].innerHTML = newHTML.getElementsByTagName('head')[0].innerHTML
                document.getElementsByTagName('body')[0].innerHTML = newHTML.getElementsByTagName('body')[0].innerHTML
                var table = $('#questions').DataTable();
                $('#questions_previous').removeClass('page-item');
                $('#questions_next').removeClass('page-item'); 
                // document.documentElement.innerHTML = data
            }).catch((error) =>{
                console.log('catch')
                    document.getElementById('module_err_msg').innerHTML = error
                    showLoadingScreen()
            })
            
        }



        var count = 0;
        function addFileInput(element) {

            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group" id="file_input_field">
                                <label>Upload Audio/Video Files <span class="text-danger">(Upload All the files at once)</span></label>
                                <input type="file" class="files form-control" name="files" multiple required>
                                <small class="form-text text-muted">
                                    Files of type mp4, wav are allowed and should be less than 60MB
                                </small><br>
                                <small class="text-danger" id="file_input_error"></small>
                            </div>`);
        }

        function addTextarea(element) {
            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group">
                                <label>Enter Text</label>
                                <textarea class="form-control text_content" name="text[]" rows="3" required></textarea>
                                <small class="text-danger"></small>
                            </div>`);
        }

        function addLinkInput(element) {
            var id = $(element).parent().parent().parent().parent().siblings()[0];
            $(id).children().last().remove();
            $(id).append(`<div class="form-group">
                                <label>YouTube Link</label>
                                <input type="text" class="form-control link_content" name="links[]" placeholder="Enter Link" required>
                                <small class="text-danger"></small>
                            </div>`);
        }


        function addCard() {
            count++;
            $(".cards").append(`<div class="card mt-3">
                             <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <div class="dropdown">
                                        <button class="btn bg-gradient-info dropdown-toggle" type="button" id="select_content_type" data-bs-toggle="dropdown" aria-expanded="false">
                                        Select Content Type
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <li><a class="dropdown-item" onclick="addFileInput(this)">Video/Audio</a></li>
                                            <li><a class="dropdown-item" onclick="addTextarea(this)">Text</a></li>
                                            <li><a class="dropdown-item" onclick="addLinkInput(this)">YouTube Link</a></li>
                                        </ul>
                                    </div>
                                    <div class="delete_container">
                                        <a class="delete_button fs-5" onclick="deleteCard(this)"><i class="fa fa-trash text-danger"></i></a>
                                    </div>
                                </div>

                                <div id="input_type_`+ count + `">

                                </div>


                             </div>
                          </div>`);
        }

        function deleteCard(element) {
            $(element).parent().parent().parent().parent().remove();
        }
    </script>
@endsection

