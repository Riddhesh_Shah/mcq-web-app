const radio = "Radio button not selected";
const question = "Question Field is empty";
const option = "Option field is empty";
const select = "No subject selected";

var i = 1;

var questions = new Array();
var errors = [];


function addSubjects(){
    var options;

    for(var i = 0 ; i < subjects.length ; i++){
        options += `<option value="${subjects[i]["id"]}">${subjects[i]["name"]}</option>`
    }
    return options;
}

$("#add-question").click(function() {
    i++;
    $(".question-group").append(`<div>
    <div class="form-group">
        <div class="d-flex justify-content-between">
            <label for="title">Question</label>
            <div class="d-flex justify-content-between">
                <div class="form-group">
                    <select class="custom-select subject" id="test-subject">
                        <option selected value='select' >Select Subjects</option>
                    `+
                        addSubjects()
                    +`</select>
                </div>
                <div class="">
                    <button type="button" id="remove-question" class="btn btn-outline-danger ml-3">Remove Question</button>
                </div>
            </div>
        </div>
        <input type="text"
            id="question"
            name="question_${i}"
            placeholder="Enter Question"
            value = ""
            class="form-control mt-3">
    </div>
    <div class="form-group">
        <div class="container">
        <div class="row" id="question-${i}">
            <input type="hidden" name="" value="1" id="question-${i}-option-count">
            <div class="col-md-6">
                <div class="row" id="question-${i}-option-1">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <input name="q-${i}-a" type="radio" class='correct-option' id="inlineCheckbox1 question-${i}-answer-1" value="1">
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control options" name="q${i}[]" placeholder="Enter Option">
                    </div>
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <i class="fa fa-times cancel-button text-danger" id="option-1"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row" id="question-${i}-option-2">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <input name="q-${i}-a" type="radio" class='correct-option' id="inlineCheckbox1 question-${i}-answer-2" value="2">
                    </div>
                    <div class="col-md-10">
                        <input type="text" class="form-control options" name="q${i}[]" placeholder="Enter Option">
                    </div>
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <i class="fa fa-times cancel-button text-danger" id="option-2"></i>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="form-group d-flex justify-content-end">
        <button type="button" id="add-option-${i}" class="add-option btn btn-outline-success">Add Options</button>
    </div>
    <hr>
</div>`);
});

$(".question-group").click(function(e) {
    if (Object.values(e.target.classList).indexOf("add-option") != -1) {
        $question_id = e.target.id.substr(11);
        $data = $("#question-" + $question_id).children().length-1;
        $optionCount = $data;
        $("#question-" + $question_id).append(`<div class="col-md-6 mt-3">
        <div class="row" id="question-${$question_id}-option-${++$optionCount}">
            <div class="col-md-1 d-flex justify-content-center align-items-center">
                <input name="q-${$question_id}-a" type="radio" class='correct-option' id="inlineCheckbox1" value="${$optionCount}">
            </div>
            <div class="col-md-10">
                <input type="text" class="form-control options" name="q${$question_id}[]" placeholder="Enter Option">
            </div>
            <div class="col-md-1 d-flex justify-content-center align-items-center">
                <i class="fa fa-times cancel-button text-danger" id="option-1"></i>
            </div>
        </div>
    </div>`);
    } else if (
        Object.values(e.target.classList).indexOf("cancel-button") != -1
    ) {
        $(e.target.parentElement.parentElement.parentElement).remove();
    } else if (e.target.id === "remove-question") {
        $(
            e.target.parentElement.parentElement.parentElement.parentElement.parentElement
        ).remove();
    }
});

$("#submit-questions").click(function(e){
    errors = [];
    var questions = new Array;
    var number_of_questions = $(".question-group").children().length;
    var parent = $(".question-group").children();
    for(var i = 0 ; i < number_of_questions ; i++){
        // console.log(getQuestion(parent[i]));
        // console.log(getOptions(parent[i]));
        // console.log(getSubject(parent[i]));
        $question = getQuestion(parent[i]);
        $options = getOptions(parent[i]);
        $module = getModule(parent[i]);
        if(!hasErrors()){
            questions.push(new Array($question, $options, $module));
        }
    }
    if(hasErrors()){
        emptyStatus();
        for(var j = 0 ; j < errors.length ; j++){
             $("#status").append(`
             <div class='col-md-12'>
                 <div class='alert alert-danger alert-dismissible fade show' role='alert'>
                     ${errors[j]}
                     <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                         <span aria-hidden='true'>&times;</span>
                     </button>
                 </div>
             </div>`);
        }
        return false;
     }else{
        alert()
        errors = [];
        emptyStatus();
        console.log(questions);
        $.ajax({
            url: "http://localhost:8000/questions",
            method:"POST",
            data:{
                questions : JSON.stringify(questions),
                _token : $("input[name='_token']").val(),
            },
            success:function(result){
                console.log(result);
                if(result == "success"){
                    window.location.reload(true);
                }
            }
        });
     };
});

function getQuestion(parent){
    console.log(parent);
    $question = $(parent).find("#question").val();
    if(!$question){
        errors.push(question);
        return false;
    }
    return $question;
}

function getOptions(parent){
    var options = new Array();
    var $options = $(parent).find(".options");
    var $correct_options = $(parent).find(".correct-option");
    console.log($correct_options);
    var $correct_option;
    console.log($options)
    $correct_option = "";
    for(var i = 0 ; i < $options.length ; i++){
        $option = $options[i].value;
        if(!$option){
            errors.push(option);
            return;
        }else{
            options.push($option);
        }
        if(isChecked($correct_options[i])){
            $correct_option = $options[i].value;
        }
    }
    if($correct_option === ""){
        errors.push(radio);
        return false;
    }
    return new Array(options, $correct_option);
}

function getModule(parent){
    var $module = $($(parent).find(".module")).val();
    if($module === "select"){
        errors.push(select);
        return;
    }
    return $module;
}
function isChecked(option){
    if(option.checked === true){
        return true;
    }
    return false;
}

function hasErrors(){
    return errors.length > 0;
}

function emptyStatus(){
    $("#status").empty();
}
/**
 * questions : {
 *  {
 *    statement : "dksndkns",
 *    options : {
 *      1:"sknksnd",
 *      2:"kdnckln"
 *    },
 *    correct_option : kldjlkndklc
 *  }
 * }
 */



