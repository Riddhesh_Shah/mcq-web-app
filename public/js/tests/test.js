// onload="noBack();" 
// onpageshow="if (event.persisted) noBack();"
// window.onbeforeunload = function() { alert() };
// $(document).delegate(".option", "click", function(e){
//     $($(this).children()[2]).children()[0].checked = true;
// });
window.addEventListener("load", function(){

    var basePath = window.origin;
    var filePath = "/tests/test/question";
    $.ajax({
        url: basePath+filePath,
        method:"POST",
        data:{
            temp : "temp",
            _token:$("input[name='_token']").val()
        },
        success:function(result){
            $("#question-holder").html(result);
        }
    });
});

$("#next").on("click", function(){

    var basePath = window.origin;
    var filePath = "/tests/test/question";
    $.ajax({
        url: basePath+filePath,
        method:"POST",
        data:{
            next : "next",
            _token:$("input[name='_token']").val()
        },
        success:function(result){
            $("#question-holder").html(result);
        }
    });
});


$("#previous").on("click", function(){

    var basePath = window.origin;
    var filePath = "/tests/test/question";
    $.ajax({
        url: basePath+filePath,
        method:"POST",
        data:{
            previous : "previous",
            _token:$("input[name='_token']").val()
        },
        success:function(result){
            $("#question-holder").html(result);
        }
    });
});


$("#container").on("click", "#save", function(){
    var question_id = $("#question-id").val();
    var option_id = getSelectedOption();
    var basePath = window.origin;
    var filePath = "/tests/test/question";
    $.ajax({
        url: basePath+filePath,
        method:"POST",
        data:{
            save_and_proceed : "save_and_proceed",
            question_id:question_id,
            option_id:option_id,
            _token:$("input[name='_token']").val()
        },
        success:function(result){
            $("#question-holder").html(result);
        }
    });
})

function getSelectedOption(){
    for(var i = 0 ; i < $(".options").length ; i++){
        if($(".options")[i].checked === true){
            return $(".options")[i].value;
        }
    }
}

$("#container").on("click", '.nav-btn',function(){
    var question_jump = $(this).children()[0].innerHTML.trim();
    var basePath = window.origin;
    var filePath = "/tests/test/question";
    $.ajax({
        url: basePath+filePath,
        method:"POST",
        data:{
            question_jump : question_jump,
            _token:$("input[name='_token']").val()
        },
        success:function(result){
            $("#question-holder").html(result);
        }
    });
});

