<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function modules(){
        return $this->hasMany(Module::class);
    }

    public function isApproved(){
        return $this->is_approved == "1";
    }
}
