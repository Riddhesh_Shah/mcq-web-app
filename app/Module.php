<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    public function questions(){
        return $this->hasMany(Question::class);
    }

    public function tests(){
        return $this->belongsToMany(Test::class);
    }

    public function subject(){
        return $this->belongsTo(Subject::class, 'subject_id');
    }
}
