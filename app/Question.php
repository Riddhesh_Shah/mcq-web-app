<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\AssignOp\Mod;

class Question extends Model
{
    protected $fillable = ["id",  'statement', 'module_id', "user_id"];
    public function options(){
        return $this->hasMany(Option::class);
    }

    public function subject(){
        return $this->belongsTo(Subject::class);
    }
    public function module(){
        return $this->belongsTo(Module::class);
    }

    public function tests(){
        return $this->belongsToMany(Test::class)->withTimestamps();
    }

    public function owner(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function getCheckedOption($test_user_id, $option_id){
        $option = DB::table("question_test_user")->where("question_id",$this->id)->where("test_user_id", $test_user_id)->first();
        if(!$option){
            return ' ';
        }else if($option->option_id === $option_id){
            return 'checked';
        }
    }

    public function isAnswerSelected($test_user_id){
        $question = DB::table("question_test_user")->where("question_id",$this->id)->where("test_user_id", $test_user_id)->first();
        return $question->option_id != null;
    }

    public function getCorrectOption(){
        foreach($this->options as $option){
            if($option->is_correct == "1"){
                return $option;
            }
        }
    }
}
