<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    use SoftDeletes;
    public $guarded = [];

    public function questions(){
        return $this->belongsToMany(Question::class)->withTimestamps();
    }

    public function modules(){
        return $this->belongsToMany(Module::class);
    }

    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function owner(){
        return $this->belongsTo(User::class,"user_id");
    }

    public function generateRandomTest(Subject $subject, $marks){

    }

    public function generateTest($name, Subject $subject, $time, $marks, Question $questions){
        $test = Test::create([
            'name' => $name,
            'total_marks' => $marks,
            'duration' => $time,
            'subject' => $subject->id,
        ]);

    }

    public function questionsCount(){
        return count($this->questions);
    }

    public function answeredQuestions($test_user_id){
        $user_id = auth()->user()->id;
        $count = DB::select("SELECT COUNT(id) as count FROM `question_test_user` WHERE test_user_id = $test_user_id AND option_id != 'null'")[0]->count;
        return $count;
    }

    public function isFirstQuestion(){
        return session()->get("count") == "0";
    }

    public function isLastQuestion(){
        return session()->get("count") == $this->questionsCount()-1;
    }

    public function getUnAnsweredQuestionCount($test_user_id){
        return intval($this->questionsCount(), 10) - intval($this->answeredQuestions($test_user_id), 10);
    }

    public static function getMarksObtained($test_user_id){

        $questionsWithOptions = DB::table('question_test_user')->where('test_user_id', $test_user_id)->get();
        // dd($questionsWithOptions);
        $marks = 0;
        foreach($questionsWithOptions as $questionWithOption){
            if($questionWithOption->option_id == Question::find($questionWithOption->question_id)->getCorrectOption()->id){
                $marks++;
            }
        }

        return $marks;

    }

    public static function updateStatus($test_user_id, $status){
        DB::table('test_user')->where('id',$test_user_id)->update([
            'status' => $status,
        ]);
    }

    public static function updateMarks($test_user_id, $marks){
        DB::table('test_user')->where('id',$test_user_id)->update([
            'marks_obtained' => $marks,
        ]);
    }

}
