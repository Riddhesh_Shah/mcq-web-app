<?php

namespace App\Http\Middleware;

use Closure;

class VerifyTest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(session()->has("test") && session()->has("test_id")){
            return redirect(route('tests.test'));
        }
        return $next($request);
        
    }
}
