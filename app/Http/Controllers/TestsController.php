<?php

namespace App\Http\Controllers;

use App\Module;
use App\Notifications\TestSubmitted;
use App\Question;
use App\Subject;
use App\Test;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $test;
    protected $option_id;
    protected $errorHtml;

    public function index()
    {
        if(auth()->user()->role == "user"){
            $tests = Test::where('is_random', '0')->get();
            $subjects = Subject::where('is_approved',"1")->get();
            return view('tests.index', compact([
                'tests',
                'subjects'
            ]));
        }
        $tests = Test::where('is_random', '0')->where("user_id", auth()->user()->id)->get();
        $subjects = Subject::where('is_approved',"1")->get();
        return view('tests.view-hosted-tests', compact([
            'tests',
            'subjects'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role != "user"){
            $questions = Question::all();
            $subjects = Subject::all();
            $modules = Module::all();
            return view('tests.create', compact([
                'questions',
                'modules',
                'subjects'
            ]));
        }
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $modules = $request->modules;
        $marks = $request->marks;
        $questions = [];
        $totalMarks = 0;

        for($i=0;$i<count($modules);$i++){
            $question_array = Module::where("name",$modules[$i])->first()->questions()->limit($marks[$i])->get();
            for($j=0;$j<count($question_array);$j++){
                array_push($questions, $question_array[$j]);
            }
            $totalMarks += $marks[$i];
        }
        //dd($questions);

        $module = Module::where("name",$modules[0])->first();

        $test = Test::create([
            "name" => $request->name,
            "duration" => $request->duration,
            "total_marks" => $totalMarks,
            "user_id" => auth()->user()->id
        ]);

        $test->modules()->attach($module->id);

        for($i=0;$i<count($questions);$i++){
            $test->questions()->attach($questions[$i]);
        }

        // $test = Test::create([
        //     'name' => $request->name,
        //     'duration' => $request->time,
        //     'module_id' => $request->module_id,
        //     'total_marks' => count($request->options),
        //     'user_id' => auth()->user()->id,
        // ]);

        // foreach ($request->options as $question_id) {
        //     $question_id = Question::where("statement", $question_id)->first()->id;
        //     $test->questions()->attach($question_id);
        // }

        return redirect(route('tests.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        return view('tests.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        // dd(count($test->users));
        if(count($test->users) > 0){
            session()->flash("error", "Test cannot be deleted");
            return redirect()->back();
        }
        $test->delete();
        session()->flash("success", "Test deleted successfully");
        return redirect()->back();
    }

    /**
     * This function is used to get all the 'Previous' tests given by user
     */
    public function previous(Test $test)
    {
        // $tests = auth()->user()->tests();
        $user_id = auth()->user()->id;
        $informations = DB::table("test_user")->where("user_id", $user_id)->orderByDesc("id")->paginate(5);
        return view('tests.previous', compact([
            'informations'
        ]));
    }

    /**
     * This function is used to get the complete data about the given 'Previous' test
     */
    public function view($test_user_id)
    {
        $test_user = DB::table("test_user")->where("id", $test_user_id)->first();
        $questions_test_user = DB::table("question_test_user")->where("test_user_id", $test_user_id)->get();
        return view('tests.view', compact([
            'questions_test_user',
            'test_user',
        ]));
    }

    /**
     * This function is used to get data for 'Random' Test and then store it in the session and then redirect to test
     */
    public function createRandomTest(Request $request)
    {
        $test = $this->getRandomTest($request->subject_id, $request->marks);
        session()->put('test_id', $test->id);
        return redirect(route('tests.test'));
    }

    /**
     * This function is used to get data for 'Hosted' Test and then store it in the session and then redirect to test
     */
    public function createHostedTest(Request $request)
    {
        $test = Test::findOrFail($request->test_id);
        session()->put('test_id', $test->id);
        return redirect(route('tests.test'));
    }

    /**
     * This function is used to get the complete data of test that the user is givimg to the test page
     */
    public function test()
    {
        // if(session()->has('test_id') && session()->has('test') && session()->has('test_user_id')) {
        //     session()->remove('test_user_id');
        //     session()->remove('count');
        //     session()->remove('maxCount');
        //     session()->remove('test');
        //     session()->remove("time");
        //     return redirect(route("tests.index"));
        // }

        if (!(session()->has('test_id') && session()->has('test_user_id'))) {
            $test = Test::findOrFail(session()->get("test_id"));
            // if($this->test->is_random == '1'){
            //     $this->test = ;
            // }else{
            //     $this->test = $test;
            // }
            $test_user_id = DB::table('test_user')->insertGetId(['test_id' => $test->id, 'user_id' => auth()->user()->id]);
            $test_id = $test->id;
            $questions = $test->questions;
            //Code to Put cookie of Time
            $timeInSeconds = intval($test->duration, 10) * 60;
            session()->put('time', "$timeInSeconds");

            foreach ($questions as $question) {
                DB::table('question_test_user')->insert([
                    'test_user_id' => $test_user_id,
                    'question_id' => $question->id,
                ]);
            }
            session()->put('test_user_id', $test_user_id);
            session()->put('test', "active");
            return view('tests.test');
        } else if (session()->has('test_id') && session()->has('test_user_id')) {
            return view('tests.test');
        }
    }

    /**
     * This function is used to get all questions related to the test which is going on
     */
    public function getQuestion(Request $request)
    {
        // $errorHtml = "";
        if ($request->save_and_proceed) {
            $this->errorHtml = "";
            $this->test = Test::find(session()->get('test_id'));
            $test_user_id = session()->get('test_user_id');

            DB::table('question_test_user')->where('test_user_id', $test_user_id)->where('question_id', $request->question_id)->update(['option_id' => $request->option_id]);

            $maxCount = count(Test::find(session()->get('test_id'))->questions) - 1;
            if ($request->option_id != null) {
                $count = intval(session()->get('count'), 10) + 1;
            } else {
                $this->errorHtml = "<small class ='text-danger'>No option selected - Cannot Save & Proceed</small>";
                $count = intval(session()->get('count'), 10);
            }
            $question = $this->test->questions()->skip(session()->get('count'))->take(1)->first();
            $option = DB::table('question_test_user')->where('test_user_id', $test_user_id)->where('question_id', $question->id)->first();
            $this->option_id = $option == null ? $option : $option->option_id;
            if ($count > $maxCount) {
                $count = $maxCount;
            }
            session()->put('count', $count);
        } else {
            if (!session()->has('count')) {
                session()->put('count', 0);
                $maxCount = count(Test::find(session()->get('test_id'))->questions) - 1;
                session()->put('maxCount', $maxCount);
                $this->test = Test::find(session()->get('test_id'));
                $test_user_id = session()->get('test_user_id');
            } else {
                $this->test = Test::find(session()->get('test_id'));
                $test_user_id = session()->get('test_user_id');
                if ($request->previous) {
                    $count = intval(session()->get('count'), 10) - 1;
                    $question = $this->test->questions()->skip(session()->get('count'))->take(1)->first();
                    $option = DB::table('question_test_user')->where('test_user_id', $test_user_id)->where('question_id', $question->id)->first();
                    $this->option_id = $option == null ? $option : $option->option_id;
                    if ($count < 0) {
                        $count = 0;
                    }
                    session()->put('count', $count);
                } else if ($request->next) {
                    $maxCount = count(Test::find(session()->get('test_id'))->questions) - 1;
                    $count = intval(session()->get('count'), 10) + 1;
                    $question = $this->test->questions()->skip(session()->get('count'))->take(1)->first();
                    $option = DB::table('question_test_user')->where('test_user_id', $test_user_id)->where('question_id', $question->id)->first();
                    $this->option_id = $option == null ? $option : $option->option_id;
                    if ($count > $maxCount) {
                        $count = $maxCount;
                    }
                    session()->put('count', $count);
                }else if($request->question_jump){
                    $question = $this->test->questions()->skip($request->question_jump-1)->take(1)->first();
                    $option = DB::table('question_test_user')->where('test_user_id', $test_user_id)->where('question_id', $question->id)->first();
                    $this->option_id = $option == null ? $option : $option->option_id;
                    session()->put('count', $request->question_jump-1);
                }
            }
        }
        $question = $this->test->questions()->skip(session()->get('count'))->take(1)->first();
        $test = $this->test;
        $test_user_id = session()->get("test_user_id");
        $count = session()->get("count") + 1;
        $is_answer_selected = $question->isAnswerSelected($test_user_id);
        $option_id = $this->option_id;
        $buttonHtml = "";
        $navigationHTML = "";
        $i = 1;
        foreach($test->questions as $tempQuestion){
            $bgColor = $tempQuestion->isAnswerSelected($test_user_id) == true ? 'bg-success' :
            'bg-primary';
            $navigationHTML .= "<div class='nav-btn mr-2 mb-2 d-inline-block' style='min-width: 50px; height: 40px;'>
                <p class='$bgColor text-white p-2 text-center'>
                $i
                </p>
            </div>";
            $i++;
        }

        if (!$is_answer_selected) {
            $buttonHtml = "<button
            type='button'
            id='save'
            value='save_and_proceed'
            class='btn btn-outline-success'
            name='save_and_proceed'>Save and proceed</button>";
        } else {
            $buttonHtml = "<span>Answer Was Saved</span>";
        }

        $optionHTML = "";
        foreach ($question->options as $option) {
            $selected = $question->getCheckedOption($test_user_id, $option->id);
            $optionHTML .= "
            <div class='row option'>
                <div class='col-1 col-md-1 cs-border-right d-flex justify-content-center align-items-center'>A</div>
                <div class='col-10 col-md-10 cs-border-right p-2'>
                    <span>$option->statement</span>
                </div>
                <div class='col-1 col-md-1 d-flex justify-content-center align-items-center'>
                    <input type='radio' value='$option->id' class='options' name='option_id' id='option' $selected>
                </div>
            </div>
            ";
        }
        $html = "<form action='' method='POST'>
            <div class='card shadow' style='min-width:600px'>
            <div class='card-header primary-bg d-flex p-4 bottom-shadow'>
                <div class='row w-100'>
                    <div class='col-md-2 text-center p-4'>
                        <h5 class='question-number'>$count<span>
                                .</span></h5>
                    </div>
                    <div class='col-md-10'>
                        <h4 class='question' id='question'>$question->statement</h4>
                        <!-- <div class='d-flex justify-content-center mt-3'> <img
                        src='{{asset('img/jdbc-driver-types-large.gif')}}' alt='' width='100%'
                        height='400px'> </div> -->
                    </div>
                </div>
            </div>
            <div class='card-body mt-3'>
                <input type='hidden' name='question_id' id='question-id' value='$question->id'>
                <input type='hidden' name='test_id' id='test-id' value='$test->id'>
                <input
                    type='hidden'
                    name='test_user_id'
                    id='test-user-id'
                    value='$test_user_id'>
                <div class='container row'>
                    <div class='col-md-12 cs-border' id='options'>
                        $optionHTML
                    </div>
                </div>
            </div>
            <div class='card-footer'>
                <div class='d-flex justify-content-between'>
                    <div id='error' class='text-danger'>
                    $this->errorHtml
                    </div>
                    <div id='button-holder'>
                    $buttonHtml
                    </div>
                </div>
            </div>
        </div>
        <div class='mt-4 text-center navigation'>$navigationHTML</div>
    </form>
    ";
        echo $html;
    }

    /**
     * This function is used to redirect page for creating 'Random' test
     */
    public function random()
    {
        $subjects = Subject::where('is_approved',"1")->get();
        return view('tests.random', compact([
            'subjects',
        ]));
    }

    /**
     * This function is used to get all the 'Hosted' tests
     */
    public function getTests($subject_id)
    {
        if ($subject_id == 0) {
            $tests = Test::where('is_random', '0')->get();
        } else {
            $tests = Test::where('subject_id', $subject_id)->where('is_random', '0')->get();
        }
        $token = csrf_field();
        $html = '';
        foreach ($tests as $test) :
            $test_id = $test->id;
            $user_id = auth()->user()->id;
            $test_user_id = DB::table("test_user")->where("test_id",$test_id)->where("user_id",$user_id)->first();
            if($test_user_id == null)
                $buttonHtml = "<button type='submit' class='btn btn-outline-primary'>Give Test</button>";
            else
                $buttonHtml = "<span>Test already given</span>";
            $name = Subject::find($test->subject_id)->name;
            $route = route('tests.test', $test->id);
            $html .= "
        <div class='col-md-6 mt-4'>
            <div class='card'>
                <form action='$route' method='POST'>
                    $token
                    <div class='card-header'>
                        <span>$test->name</span>
                    </div>
                    <div class='card-body'>
                        <p><strong>Total Marks : </strong>$test->total_marks</p>
                        <p><strong>Subject : </strong>$name</p>
                        <p><strong>Duration : </strong>$test->duration Mins</p>
                    </div>
                    <div class='card-footer text-right'>
                        $buttonHtml
                    </div>
                </form>
            </div>
        </div>
        ";
        endforeach;
        echo $html;
    }

    /**
     * This function is used to get questions related to subjects selected for 'Random' page
     */
    public function getQuestions(Request $request, $module_id)
    {
        $order_by = $request->order[0]["dir"] ?? null;
        $start = $request->start;
        $length = $request->length;
        $draw = $request->draw;
        // echo $draw;
        if ($module_id == 0) {
            $questions = Question::all();
            $recordsFiltered = count($questions);
            if($length != -1){
                $questions = array_slice($questions->toArray(), $start, $length);
            }
        } else {
            $questions = Question::where('module_id', $module_id)->get();
            $recordsFiltered = count($questions);
            $questions = array_slice($questions->toArray(), $start, $length);
        }
        $totalLength = count(Question::all());
        $token = csrf_field();
        $data = [];
        foreach ($questions as $question){
            $subarray = [];
            $subarray[] = $question["statement"];
            $subarray[] = "<td></td>";
            $data[] = $subarray;
        }
        $output = [
            "draw"=>$draw,
            "recordsTotal"=>$totalLength,
            "recordsFiltered"=> $recordsFiltered,
            "data"=>$data
        ];
        echo json_encode($output);
            // $name = Subject::find($question->subject_id)->name;
            // $route = route('tests.test', $question->id);
        //     $html .= "
        //     <tr class='option'>
        //         <td>$question->statement</td>
        //         <td class = 'checkbox'>
        //             <input type='checkbox' name='options[]' class='checkbox-option' value='$question->id'>
        //         </td>
        //     </tr>
        // ";
    }

    /**
     * This function is used to get marks related to subjects selected for 'Random' page
     */
    public function getMarks($subject_id)
    {
        $subject = Subject::findOrFail($subject_id);
        $questionsCount = count($subject->questions);
        $marksArray = $questionsCount <= 10 ? [$questionsCount] : [];
        $marks = $questionsCount <= 10 ? $questionsCount : 10;
        while ($marks < $questionsCount) {
            $marksArray[] = $marks;
            $marks += 10;
        }
        $html = "<option value='0' selected>Select Marks</option>";
        foreach ($marksArray as $mark) :
            $html .= "
                <option value='$mark'>$mark</option>
            ";
        endforeach;
        echo $html;
    }

    /**
     * This function is used to generate a random test on the basis of subjects and marks selected by the user
     */
    public function getRandomTest($subject_id, $marks)
    {
        $subject = Subject::findOrFail($subject_id);
        $questions = $subject->questions->toArray();
        // dd($questions);
        shuffle($questions);

        $test = Test::create([
            'name' => 'Random Test',
            'total_marks' => $marks,
            'duration' => $marks,
            'subject_id' => $subject_id,
            'is_random' => '1',
        ]);

        for ($i = 0; $i < $marks; $i++) {
            $test->questions()->attach($questions[$i]['id']);
        }

        return $test;
    }

    /**
     * This function is used when cancel button is clicked it is used to free all the session variables
     */
    public function cancel()
    {
        $test_id = session()->get('test_id');
        $test_user_id = session()->get('test_user_id');

        $marks = Test::getMarksObtained($test_user_id);
        Test::updateMarks($test_user_id, $marks);
        Test::updateStatus($test_user_id, "incomplete");

        session()->flash('success', 'Your test was submitted successfully');
        session()->remove('test_user_id');
        session()->remove('count');
        session()->remove('maxCount');
        session()->remove('test');
        return redirect(route('tests.index'));
    }

    /**
     * This function is used when submit button is clicked it is used to free all the session variables
     */
    public function submit()
    {
        $test_id = session()->get('test_id');
        $test_user_id = session()->get('test_user_id');

        $marks = Test::getMarksObtained($test_user_id);
        Test::updateMarks($test_user_id, $marks);
        Test::updateStatus($test_user_id, "completed");

        $user = User::find(auth()->user()->id);
        // $user->notify(new TestSubmitted(session()->get('test_user_id')));

        session()->flash('success', 'Your test was submitted successfully');
        session()->remove('test_user_id');
        session()->remove('count');
        session()->remove('maxCount');
        session()->remove('test');
        return redirect(route('tests.index'));
    }

    public function getHostedTest(Request $request){
        $test = Test::find($request->test_id);
        return view("tests.view-hosted-test", compact(["test"]));
    }
}
