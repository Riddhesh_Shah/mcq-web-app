<?php

namespace App\Http\Controllers;

use App\Module;
use App\Option;
use App\Question;
use App\Subject;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::all();
        return view('questions.create', compact(['modules']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
    //    dd(((count($request->toArray())-2)/3));
        // $request = json_encode();
        // $data = json_decode($request->questions, true);
        // var_dump($data);
        for ($i = 0; $i < ((count($request->toArray())-2)/3); $i++){
            $question = Question::create([
                'statement' => request("question_" . $i),
                'module_id' => $request->module_id,
                'user_id' => auth()->user()->id,
            ]);
            for($j=0; $j<count(request("q".$i)); $j++){
                Option::create([
                    'statement' => request("q".$i)[$j],
                    'question_id' => $question->id,
                    'is_correct' => request("q-".$i."-a") == $j+1,
                ]);
            }
            
        } 


        // echo "success";
        session()->flash("success", "Questions were added successfully");
        return redirect(route('users.index'));
    }

    public function getQuestion($row)
    {
        return $row[0];
    }

    public function getOptions($row)
    {
        return $row[1][0];
    }

    public function getCorrectOption($row)
    {
        return $row[1][1];
    }

    public function getModule($row)
    {
        return $row[2];
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }

    public function generatedQuestions(Request $request){
        dd($request);
    }

}
