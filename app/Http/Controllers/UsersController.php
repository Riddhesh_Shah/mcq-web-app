<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserEditRequest;
use App\Subject;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role == "admin"){
            $users = User::paginate(10);
            $user = auth()->user();
            $tests = $user->createdTests;
            if($tests == null){
                $testsCount = 0;
            }else{
                $testsCount = count($tests->toArray());
            }
            $questions = $user->createdQuestions;
            if($questions == null){
                $questionsCount = 0;
            }else{
                $questionsCount = count($questions->toArray());
            }
            $usersCount = 0;
            foreach($tests as $test){
                $usersCount += count($test->users);
            }
            return view('users.index',compact([
                'testsCount',
                'usersCount',
                'questionsCount',
                'users'
            ]));
        }else if(auth()->user()->role == "host"){
            $user = auth()->user();
            $tests = $user->createdTests;
            if($tests == null){
                $testsCount = 0;
            }else{
                $testsCount = count($tests->toArray());
            }
            $questions = $user->createdQuestions;
            if($questions == null){
                $questionsCount = 0;
            }else{
                $questionsCount = count($questions->toArray());
            }
            $usersCount = 0;
            foreach($tests as $test){
                $usersCount += count($test->users);
            }
            return view('users.index',compact([
                'testsCount',
                'usersCount',
                'questionsCount'
            ]));
        }else{

            $subjectsWithAverageMarks = [];
            $subjects = Subject::all();
            foreach($subjects as $subject){
                $subjectsWithAverageMarks[$subject->name] = auth()->user()->getTestAverageBySubject($subject->id);
            }

            $user = auth()->user();
            $tests = $user->tests;
            $testsCount = count($tests->toArray());
            return view('users.index',compact([
                'testsCount',
                'subjectsWithAverageMarks',
            ]));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', compact(["user"]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact(["user"]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, User $user)
    {
        // dd($user);
        $request->validated();
        $user->update([
            "name" => $request->name,
            "email" => $request->email,
            "phone" => $request->phone,
            "address" => $request->address,
        ]);

        return redirect(route("users.show", auth()->user()->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
