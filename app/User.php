<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tests(){
        return $this->belongsToMany(Test::class)->withTimestamps();
    }

    public function createdTests(){
        return $this->hasMany(Test::class, "user_id");
    }

    public function createdQuestions(){
        return $this->hasMany(Question::class, "user_id");
    }

    public function getTestAverageBySubject($subject_id){
        $user = auth()->user();
        $subject = Subject::find($subject_id);
        $testCount = 0;
        $totalMarks = 0;
        $obtainedMarks = 0;
        foreach($this->tests as $test){
            // dd($test->module->subject_id);
            if($test->modules()->first()->subject_id == $subject->id){
                $testCount++;
                $totalMarks += intval($test->total_marks, 10);
                $test_user = DB::table('test_user')->where('test_id',$test->id)->where('user_id',$user->id)->first();
                $obtainedMarks += intval($test_user->marks_obtained, 10);
            }
        }
        $averagePercentage = $totalMarks == 0 ? 0 : intval(($obtainedMarks/$totalMarks) * 100, 10);
        return $averagePercentage;
    }
}
