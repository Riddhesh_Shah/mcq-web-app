<?php

use App\Option;
use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<50;$i++){
            $option = rand(0,3);
            for($j=0;$j<4;$j++){
                if($j == $option){
                    $is_correct = '1';
                }else{
                    $is_correct = '0';
                }
                Option::create([
                    'statement' => Faker\Factory::create()->word(5,10),
                    'question_id' => ($i + 1),
                    'is_correct' => $is_correct,
                ]);
            }
        }
    }
}
