<?php

use App\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'name' => "C",
        ]);

        Subject::create([
            'name' => "C++",
        ]);

        Subject::create([
            'name' => "Data Structures",
        ]);

        Subject::create([
            'name' => "Java",
        ]);

        Subject::create([
            'name' => "PHP",
        ]);

        Subject::create([
            'name' => "Python",
        ]);

        Subject::create([
            'name' => "Front End",
        ]);

        Subject::create([
            'name' => "DBMS",
        ]);

        Subject::create([
            'name' => "Networks",
        ]);

        Subject::create([
            'name' => "Maths",
        ]);

        for ($i = 0; $i<5; $i++){
            \App\Module::create([
                'name' => "Module $i",
                'subject_id' => rand(1, 7),
            ]);
        }
    }
}
