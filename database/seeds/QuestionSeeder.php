<?php

use App\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<5;$i++){
            for($j=0;$j<10;$j++){
                Question::create([
                    'statement' => Faker\Factory::create()->sentence(10,20),
                    'module_id' => ($i + 1),
                    'user_id' => rand(1,22),
                ]);
            }
        }
    }
}
