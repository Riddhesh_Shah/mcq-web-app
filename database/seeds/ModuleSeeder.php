<?php
use App\Module;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 0; $i<5; $i++){
            Module::create([
                'name' => "Module " . ($i+1),
                'subject_id' => rand(1, 7),
            ]);
        }
    }
}
