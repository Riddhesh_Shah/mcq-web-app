
<?php

use App\Module;
use App\Test;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i=0;$i<20;$i++){
            $module = Module::find(rand(1,5));
            $test = Test::create([
                'name' => $i%4 == 0 ? "Random Test" : ucfirst(Faker\Factory::create()->words(1,3)) . " Test",
                'total_marks' => '5',
                'duration' => '10',
                'is_random' => $i%4 == 0 ? "1" : "0",
                'user_id' => $i%4 == 0 ? null : rand(1,22),
            ]);
            $test->modules()->attach($module->id);
            $questions = $module->questions->toArray();
            shuffle($questions);
            for($j=0;$j<5;$j++){
                $question = $questions[$j];
                $test->questions()->attach($question["id"]);
                $test->users()->attach(rand(1,30), ["marks_obtained" => rand(1,10)]);
            }
        }
    }
}
