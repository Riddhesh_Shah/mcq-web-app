<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Riddhesh Shah",
            "email" => "riddheshshah@gmail.com",
            "password" => Hash::make("rs123789"),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            "phone" => Faker\Factory::create()->phoneNumber(),
            "address" => "Mulund",
            'role' => 'host',
        ]);

        User::create([
            "name" => "Soham Karalkar",
            "email" => "sohamkaralkar@gmail.com",
            "password" => Hash::make("sk123789"),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            "phone" => Faker\Factory::create()->phoneNumber(),
            "address" => "Nahur",
            'role' => 'admin',
        ]);

        for($i=0; $i<28; $i++){

            /**
             * FirstName & LastName are created differently as they are used in the whole user object different times and in different ways
             */
            $firstName = Faker\Factory::create()->firstName();
            $lastName = Faker\Factory::create()->lastName();

            User::create([            
                "name" => $firstName . " " . $lastName,
                "email" => strtolower($firstName . $lastName . "@gmail.com"),
                "password" => Hash::make("password"),
                'email_verified_at' => now(),
                'remember_token' => Str::random(10),
                "phone" => Faker\Factory::create()->phoneNumber(),
                "address" => Faker\Factory::create()->city(),
                'role' => 'user',
            ]);
        }
    }
}
