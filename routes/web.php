
<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TestsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::middleware(['auth'])->group(function(){
    Route::get("/", function(){
        return redirect(route("users.index"));
    });


    Route::get('/home', [\App\Http\Controllers\HomeController::class, "index"])->name('home');

    Route::resource('users', \App\Http\Controllers\UsersController::class);

    Route::get('tests/test',[TestsController::class, "test"])->name("tests.test");

    Route::post('tests/test/random/create',[TestsController::class, 'createRandomTest'])->name("tests.create.random");
    Route::post('tests/test/hosted/create',[TestsController::class, 'createHostedTest'])->name("tests.create.hosted");


    Route::post('tests/test/submit', [TestsController::class, 'submit'])->name("tests.submit");
    Route::post('tests/test/cancel', [TestsController::class, 'cancel'])->name("tests.cancel");

    Route::post('tests/test/question', [TestsController::class, 'getQuestion']);

    Route::get('tests/get/test/{subject_id}',[TestsController::class, 'getTests'])->name('tests.getTests');
    Route::get('tests/get/question/{subject_id}', [TestsController::class, 'getQuestions'])->name('tests.getQuestions');
    Route::get('tests/get/marks/{subject_id}',[TestsController::class, 'getMarks'])->name('tests.getMarks');
});
Route::get("questions/generatedQuestions",function(){
return view('questions.generatedQuestions');
});
Route::post('tests/hosted/test', [TestsController::class,"getHostedTest"])->name("tests.hosted")->middleware(['auth', 'blockuser']);
Route::get('tests/previous',[TestsController::class, 'previous'])->name('tests.previous')->middleware(['auth', 'blockhost']);
Route::post('tests/{test}/view',[TestsController::class, 'view'])->name('tests.view')->middleware(['auth', 'blockhost']);
Route::get('tests/random',[TestsController::class, 'random'])->name("tests.random")->middleware(['auth', 'blockhost']);

Route::resource('questions',\App\Http\Controllers\QuestionsController::class)->middleware(['auth', 'blockuser']);
Route::get("subjects", [\App\Http\Controllers\SubjectsController::class, "index"])->name("subjects.index")->middleware(['auth', 'admin']);
Route::get("subjects/create", [\App\Http\Controllers\SubjectsController::class, "create"])->name("subjects.create")->middleware(['auth', 'blockuser']);
Route::post("subjects", [\App\Http\Controllers\SubjectsController::class, "store"])->name("subjects.store")->middleware(['auth', 'blockuser']);
Route::put("subjects/{subject}", [\App\Http\Controllers\SubjectsController::class, "update"])->name("subjects.update")->middleware(['auth', 'admin']);
Route::delete("subjects/{subject}", [\App\Http\Controllers\SubjectsController::class, "destroy"])->name("subjects.destroy")->middleware(['auth', 'admin']);

Route::get("modules", [\App\Http\Controllers\ModulesController::class, "index"])->name("modules.index")->middleware(['auth', 'admin']);
Route::get("modules/create", [\App\Http\Controllers\ModulesController::class, "create"])->name("modules.create")->middleware(['auth', 'blockuser']);
Route::post("modules", [\App\Http\Controllers\ModulesController::class, "store"])->name("modules.store")->middleware(['auth', 'blockuser']);
Route::put("modules/{module}", [\App\Http\Controllers\ModulesController::class, "update"])->name("modules.update")->middleware(['auth', 'admin']);
Route::delete("modules/{module}", [\App\Http\Controllers\ModulesController::class, "destroy"])->name("modules.destroy")->middleware(['auth', 'admin']);

Route::resource('tests',TestsController::class)->middleware(['test', 'auth']);

// Route::get("questions/generatedQuestions", function(){
//     
// });

