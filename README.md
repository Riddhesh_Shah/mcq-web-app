# MCQ Web App

## Description
<p>
This is a complete full-fledged MCQ Web App with multiple user authorities with different permissions which includes:
</p>
<ol>
<li>Admin</li>
<li>Host</li>
<li>User</li>
</ol>
<p>Permissions</p>
<ul>
<li>
Admin
<ul>
<li>View Student Detail</li>
<li>Create a test</li>
<li>Add / View Questions</li>
<li>Add Subject</li>
</ul>
</li>

<li>
Host
<ul>
<li>View Student Detail</li>
<li>Create a test</li>
<li>Add / View Questions</li>
<li>Add Subject</li>
</ul>
</li>

<li>
User
<ul>
<li>View Dashboard</li>
<li>Give Hosted / Random Test</li>
<li>View Previous Tests</li>
<li>View / Edit Profile</li>
</ul>
</li>
</ul>

## Output Screens

### Student Index
![img.png](img.png)

### Give Test
![img_1.png](img_1.png)

### Actual Test Page
![img_2.png](img_2.png)

### Previous Tests
![img_3.png](img_3.png)

### Prvious Score
![img_4.png](img_4.png)
